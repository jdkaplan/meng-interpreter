Instructions for trying it out
==============================

First off, you'll probably want to make a new virtualenv based on Python 3,
because this language doesn't have a name, and I don't know what to name my
packages.  All of the instructions below assume that your current working
directory is the root of this repository and that you're using Python 3.

`python -m src` drops you into a REPL.

`python -m src foo` runs the file `foo` and exits.  Add the `--interactive` or
`-i` flag to drop into in a REPL afterward.  Add `--settings bar` or `-s bar` to
set the settings from `bar` before running.  Example settings files can be found
in the `settings` directory.

If you want to run the tests, you'll definitely want a virtualenv.  Install
pytest with `pip install pytest`.  Then, to make all the imports in the tests
work, install the language with `pip install -e.`.  Now, you can run pytest with
`py.test` as usual.  If you want to see the statistics about the code coverage
of the tests, you'll also need to `pip install pytest-cov` and add `--cov=src`
to the pytest flags.  The `./test` shell script runs pytest with the coverage
plugin and passes any supplied arguments along to pytest.  For example, you can
run just the generator tests with `./test tests/test_generator.py`.

You can shortcupt the manual installation of these python packages by running
`pip install -r requirements.txt` with the requirements file provided.


Language Quickstart
===================

See the file `doc/quickstart.prog`.
