\chapter{Parser Generator}
\label{chap:generator}

Our language and interpreter framework provide a high level of grammatical
flexbility to the student using the language and to the instructor defining the
interpreter.  Our requirements for the parser generator were decided by our need
to make programmatic changes to the parser at runtime and during interpreter
startup.  The runtime changes to the parser, in turn, required that our parser
was able to handle parsing a program incrementally, waiting for new rules to be
defined before parsing the parts of the program that used them.  Our grammar
also contains some rules that are not easily expressed using regular
expressions, so the parser also needed to support these irregular rules.

Runtime changes to the parser are necessary to accomodate runtime macros.  As we
execute a program, macro definition statements can be used to define new syntax
rules that will be added to the parser and be available for use immediately.
Start time changes are necessary to accomodate custom operators and other
startup settings.

We need more complex rules than regular expressions will allow because of how we
chose to specify identifiers.  Our identifiers are any number of non-whitespace
characters that are not reserved words and do not contain any operator symbols.
This rule, especially in the context of runtime-defined keywords, cannot be
easily expressed as a regular expression.

We also need to be able to parse a prefix of the program text because we cannot
depend on knowing all syntax structures before interpretation begins.  One of
the problems with interpreting a non-homogeneous language with runtime macros is
that the rules for parsing later statements may not exist until partway through
the interpretation of the program.

Based on these requirements, we evaluated parser generators based on their
affordances for the kinds of grammar flexibility we needed:

\begin{enumerate}
\item The parser should support changes at runtime.
\item The parser should support changes at start time.
\item The parser should support parsing a prefix of the program text.
\item The parser should support rules beyond regular expressions.
\end{enumerate}

\section{Other Parser Generators}

We considered four existing parser generators (PLY, PyPEG, Grako, and Arpeggio)
but ultimately decided on a custom parser generator.  Most parser generators
work by reading in a specification of the grammar (usually in EBNF or PEG
syntax) and generating code or parse tables that can be used as a parser.  The
rules that can be defined are usually restricted to EBNF, PEG, or regular
expressions, although some generators allow for custom rules.  These parsers are
intended to be used to parse entire files containing programs (or other text)
into one AST.

These parsers lack support for runtime changes because the generated tables are
in an opaque binary format, and the generated code is difficult to extend.  It
is possible to work around this limitation, however, by adding new rules and
re-generating the parser.

These parsers also lack support for incremental parses because they assume that
there is a single rule that exists to match the entire program text.  This
results in parsers that produce an error if the entire file does not match the
rules of the language.  Without a partial parse, the only way to support macros
is to require that all macros be defined before any other kind of statement
executes, which turns our runtime macros into compile-time macros.


\section{Custom Parser Generator}

We designed our custom parser generator around the features described earlier:
allowing changes at both start and runtime, allowing incremental parses, and
allowing arbitrarily-complicated user-defined rules.  Our approach was
structured around the using parser combinators to define complex rules, but we
retained the ability to define parser rules without using the combinators.  We
allow programmatic changes by defining the grammar using Python and exposing
some of its internal data structures to the other parts of the interpreter.  We
support incremental parses by allowing the parser to emit a match without
encountering an end-of-file (EOF).  In the rest of this section, we describe the
data structures and operation of our parser generator framework.

\subsection{Data Structures}

Our parser generator is centered around three kinds of objects: \texttt{Match}
objects, \texttt{Parser} functions, and \texttt{Rule} functions.  We provide a
library of simple primitive rules, rule combinators, and a means of turning
\texttt{Rule}s into \texttt{Parser}s.

\paragraph{Match Objects}
For our parser to support incremental parsing, we needed to know how much of the
program text was matched and the location of that program fragment in the entire
text.  We also needed to support incremental AST creation during a parse, which
we implemented as an extra field attached to a match.

This resulted in a \texttt{Match} object with the following attributes:

\begin{itemize}
\item \texttt{text}: the fragment of the source that was matched
\item \texttt{position}: the starting position of this match in the source text
\item \texttt{data}: unrestricted storage that we used for AST creation
\end{itemize}

\paragraph{Parser Functions}
A \texttt{Parser} is an infinite Python generator that takes two arguments: the
source text and a start position in that text.  The generator will initially
emit \texttt{Match} objects representing the portions of the text that it
recognizes starting at the specified position.  To support backtracking in the
parser, all possible \texttt{Match}es will be emitted in the order that they
should be considered.  When a \texttt{Parser} has exhausted the set of
\texttt{Match} objects it can produce, it continues by emitting the infinite
sequence of \texttt{None}s, at which point we consider the parse to have failed.
Successsful parses are those that emit at least one \texttt{Match}.

\paragraph{Parser Rule Functions}
A \texttt{Rule} has exactly the same interface as a \texttt{Parser}, except that
it need not be infinite.  This allows us to define \texttt{Rule}s that only emit
success cases, and we can turn them into proper \texttt{Parser}s by affixing the
infinite sequence of \texttt{None}s to its generated \texttt{Match}es.  This
simplifies the grammar by allowing us to omit a boilerplate \texttt{while True:
  yield None} at the end of every function in the grammar.


\subsection{Rule Creation}

Our parser generator was implemented as a library for specifying common
\texttt{Rule} patterns in this framework.  This library provides some functions
for creating primitive rules (which deal with matching specific strings in the
text) and some combinator functions (which deal with matching patterns or
combinations of other rules).  Each of these functions takes an optional
callback function that is used to initialize the \texttt{data} field on each
emitted \texttt{Match}.

\subsubsection{Rule Primitives}

We defined two primitive rules: \texttt{Raw} and \texttt{Regex}.  These are used
when matching simple strings or patterns in the text.

\paragraph{Raw}
A \texttt{Raw} rule emits at most one \texttt{Match} representing an exact
string match at the specified position.  The required argument when creating a
\texttt{Raw} rule is the exact string to match.  The callback function is called
on the string that was matched.  These rules are primarily used for keywords,
operator symbols, and delimiters.

\paragraph{Regex}
A \texttt{Regex} rule emits at most one \texttt{Match} representing the results
of using Python's \texttt{re.match} on a slice of the source string starting at
the specified position.  The required argument when creating a \texttt{Regex}
rule is the regular expression to match.  The callback function is called on the
Python regular expression match object returned by the regular expression test.
These rules are primarily used for numeric literals, string literals, and
whitespace.

\subsubsection{Rule Combinators}

We defined rule combinators in order to easily specify common patterns in our
grammar.  These patterns include optional syntax, repeated syntax, and syntax
alternatives, among others.

\paragraph{Optional}
The \texttt{Optional} combinator is used in places where certain syntax is
allowed but not required, such as the alternative clause in a conditional.  This
combinator takes one rule as its required argument and emits either one or two
\texttt{Match}es, so it always succeeds.  If the provided rule parses
sucessfully, the first object emitted is a \texttt{Match} representing that
match.  In this case, the callback function is passed the \texttt{data} from the
original \texttt{Match}.  Regardless of whether the rule parses sucessfully, the
next \texttt{Match} emitted represents the match of the empty string, becasue
the empty string is always a possible match for an \texttt{Optional} rule.  In
this case, the callback function is passed \texttt{None}.

\paragraph{Choice}
The \texttt{Choice} combinator is used when alternatives are allowed in syntax
and exactly one of the alternatives should be used, such as having multiple
kinds of numeric literals.  This combinator takes a list of rules as its
required argument, representing the possible options in the order that they
should be tried.  Each option is tried in order, and all \texttt{Match}es from
the parse of one option are emitted before trying the next option.
\texttt{Choice} succeeds when at least one of its options succeeds.  The
callback function is passed the \texttt{data} from the \texttt{Match} that is
emitted.

\paragraph{Sequence}
The \texttt{Sequence} combinator is used when a there are rules should match in
a specified order, such as the tokens required for an assignment statement.
This combinator takes a list of rules as its required argument representing the
necessary rules to match in order.

When parsing a \texttt{Sequence}, we iterate over each of the rules, starting
the parse for each rule from the end of the match from the previous rule.  If
any rule in the sequence fails to parse, we backtrack by using the next match
for the previous rule and retrying any later rules from there.  The parse fails
if backtracking exhausts all possibilities for the first rule, leaving no
possible matches for the sequence.  The parse succeeds when at least one
compatible set of matches is found for every rule in the sequence.  The callback
function is passed a list containing the \texttt{data} from each \texttt{Match}
in the sequence.

We found that we often needed whitespace in between the rules of our
\texttt{Sequence}s.  Assuming a whitespace rule \texttt{ws} and some set of
interesting rules \texttt{r1}, \texttt{r2}, and \texttt{r3}, this resulted in
messy definitions like \texttt{Sequence([ws, r1, ws, r2, ws, r3])}, when we
would rather define \texttt{Sequence([r1, r2, r3])} and have the whitespace
match automatically.  Therefore, \texttt{Sequence} takes one optional parameter
called \texttt{skip\_whitespace} which specifies the pattern of text to skip,
defaulting to \texttt{Regex(\textbackslash{}s*)}.  If \texttt{skip\_whitespace}
is truthy, we try to match it before each rule in the sequence.  Any matches
from \texttt{skip\_whitespace} are omitted when passing the list of data to the
callback function.  It is important to note that \texttt{skip\_whitespace} must
be able to match the empty string for that whitespace to be considered optional.

\paragraph{Star and Plus}

The \texttt{Star} and \texttt{Plus} combinators (named for the Kleene star and
plus, respectively) are used when syntax elements can be repeated multiple
times.  \texttt{Star} handles the case where the syntax can be repeated zero or
more times, while \texttt{Plus} requires at least one occurrence.  Each of these
combinators takes a single rule as its required argument representing the rule
to repeatedly match.  We assume that these combinators will be used to match as
many repetitions as possible, so the matches that are emitted starting with the
one with the greatest number of repetitions and ending with the empty match.
The callback function is passed a list containing the \texttt{data} from each
\texttt{Match}, in order, using the empty list for the empty match.

\paragraph{SeparatedStar and SeparatedPlus}

The \texttt{SeparatedStar} and \texttt{SeparatedPlus} combinators are used when
a repeated syntax element is separated by another syntax element, such as
function arguments separated by commas.  \texttt{SeparatedStar} handles the case
for zero or more repetitions, while \texttt{SeparatedPlus} requires at least one
occurrence of the repeated element.

These combinators take two rules as their required arguments representing the
main syntax to match and the separator to match.  When parsing, we alternate
matching the main rule and the separator rule until no more matches can be made.
These matches are emitted in order from greatest to least repetitions, in the
same way as \texttt{Star}.  The callback function is passed a list containing
the \texttt{data} from each \texttt{Match} emitted by the main rule, omitting
the data for the separators by default.  This combinator takes two optional
arguments: \texttt{keep\_separators} (in the case that the separators should be
used in making the AST) and \texttt{skip\_whitespace} (which operates exactly as
in \texttt{Sequence}).

Usually, the separator rule matches something uninteresting, such as a comma or
newline, and callback functions are written only for the main matches.  However,
knowledge about the separator itself may be required during a parse, such as
knowing whether the separator was a comma or a semicolon within a MATLAB-style
matrix literal, so setting \texttt{keep\_separators} to \texttt{True} will
retain the separator match data in the list passed to the callback function.

Like in \texttt{Sequence}, \texttt{skip\_whitespace} is used to skip over
whitespace between rules.  In this case, \texttt{skip\_whitespace} defaults to
\texttt{None}, because whitespace is often part of the separator rule itself.

\paragraph{Single}

The \texttt{Single} combinator is included for the case where extra processing
needs to be applied to the \texttt{data} of an existing rule's \texttt{Match}.
Usually, that extra processing would simply be added to the callback function of
the associated \texttt{Rule}, but we found no simple way to achieve that when
generating macro rules (discussed in \Cref{sec:make_syntax_rule}).  The
\texttt{Single} combinator takes one rule as its required argument and emits the
same matches that that rule emits, but with the extra callback function applied
to the \texttt{data} in that \texttt{Match}.

\section{Summary}

All the primitives and combinators in our parser generator library (except for
\texttt{Single}) have equivalents in other parser generators, whether based on
EBNF or PEG.  However, standard EBNF or PEG parser generators are very fast.  We
trade off that parsing speed for more expressiveness in the kinds of grammars
that can be defined.  This allows us to define irregular rules, stateful
parsers, and regular Python functions within our grammar.


Parser generators that output parse tables or generated code are notoriously
difficult to debug.  Because our a parser in our framework is just a Python
program, a grammar can be debugged without the need of special tools or
knowledge beyond standard Python techniques.  The callback functions are a
natural place to perform logging to determine parser progress or parser state.

Some usage examples are described in \Cref{app:grammar-samples}.  In
\Cref{chap:grammar}, we use this parser generator to describe the syntax of our
programming language.
