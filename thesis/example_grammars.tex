\chapter{Sample Grammars}
\label{app:grammar-samples}

In this appendix, we present some sample grammars that can be defined using our
parser generator library (discussed in \Cref{chap:generator}).

\section{EBNF Grammar}

As an example, we have included a sketch of the implementation of a parser for
EBNF grammars (\Cref{fig:ebnf}).  This is not the most concise specification of
EBNF, but we include this form of it to show some common usage patterns.

Letters and digits are recognized by common regular expression patterns, but the
regular expression rules for matching any of a set of special characters that
are also reserved in regular expressions are complicated.  Here we make a single
\texttt{Raw} string matcher for each special character and combine them into a
\texttt{Choice} rule.  Identifiers cannot contain whitespace, and terminals
should include all characters within their enclosing quotes, so we disable
whitespace skipping within those rules.  \texttt{rhs} is an example of a
self-recursive rule, so defining it as a simple \texttt{Choice} rule would fail,
as the argument to \texttt{Choice} cannot be evaluated until \texttt{rhs} has
been defined.  The current workaround for this is to create a function that then
makes a rule (with all names resolvable by the time it is called) and then yield
all results from a parse of that rule.

\begin{figure}[htbp]
\caption{An EBNF grammar}
\label{fig:ebnf}
\begin{verbatim}
from generator import *

letter = Regex(r'[a-zA-Z]')
digit = Regex(r'\d')
symbol = Choice([Raw(s) for s in '''[]{}()<>'"=|.,;'''])
character = Choice([letter, digit, symbol, Raw('_')])
identifier = Sequence(letter, Star(Choice([letter, digit, Raw('_')])),
                      skip_whitespace=False)
terminal = Choice([
        Sequence([Raw("'"), Plus(character), Raw("'")],
                 skip_whitespace=False),
        Sequence([Raw('"'), Plus(character), Raw('"')],
                 skip_whitespace=False),
])
lhs = identifier

def rhs(source, position):
    rule = Choice([
            identifier,
            terminal,
            Sequence([Raw('['), rhs, Raw(']')]),
            Sequence([Raw('{'), rhs, Raw('}')]),
            Sequence([Raw('('), rhs, Raw(')')]),
            Sequence([rhs, Raw('|'), rhs]),
            Sequence([rhs, Raw(','), rhs]),
    ])
    yield from parse(rule, source, position)

rule = Sequence([lhs, Raw('='), rhs, Raw(';')])
ebnf = Star(rule)
\end{verbatim}
\end{figure}

This specification does not use any callback functions, so all the data
attributes on returned \texttt{Match}es are strings or lists of strings.  To
check whether a string is recognized by this grammar, we would use the generator
library's \texttt{parse} function and check whether or not it returned a valid
match:

\texttt{next(parse(ebnf, "x = 'Hello';", 0)) is not None}


\section{Palindromes}

Although arbitrary-length palindromes cannot be matched by traditional regular
expressions, many programming languages (Perl and Ruby, for example) provide
extensions to regular expressions that allow for such a ``regular expression''
to exist.  Python is not one of these languages, so an external library must be
used to access that kind of extension.  In \Cref{fig:palindrome}, we demonstrate
a parser rule that would recognize arbitrary-length palindromes that contain no
whitespace.

The implementation of \texttt{is\_palindrome} makes this rule extremely slow.
It naively checks for palindromes by verifying that the first and last
characters are the same and then recurses into the middle of the string.  This
creates a very large call stack, and creates many slices of the string in
memory.  However, we include this as an example of rules that would be difficult
(or impossible) to define in other parser generators.

\begin{figure}[htbp]
\caption{A palindrome grammar}
\label{fig:palindrome}
\begin{verbatim}
from generator import *

def is_palindrome(x):
    if len(x) < 2: return True
    if x[0] == x[-1]: return is_palindrome(x[1:-1])
    return False

def palindrome(source, position):
    for match in parse(Regex(r'\S*'), source, position):
        for i in range(len(match.text), -1, -1):
            pal = match.text[:i]
            if is_palindrome(pal):
                yield Match(pal, position, pal)
\end{verbatim}
\end{figure}


\section{Stateful Parser}

Many parser generators support parser states to simplify the implementation of
separate ``modes'' of parsing.  This parser generator also supports modes as a
special case of general parser state.  We include an example of a stateful
grammar (\Cref{fig:stateful}) that only accepts words that have been explicitly
marked as acceptable (using the \texttt{:word} syntax).  This example is
somewhat contrived; any grammars that truly need state tend to be too large or
complicated to use as an example.

This parser stores its state in the \texttt{accepted\_words} list.  The callback
function for \texttt{new\_word} adds each new word to this list.  Each time
\texttt{known\_word} is used to generate matches, it creates a new
\texttt{Choice} from all the acceptable words at that time.

\begin{figure}[htbp]
\caption{A stateful grammar}
\label{fig:stateful}
\begin{verbatim}
from generator import *

accepted_words = []

def new_word(source, position):
    def callback(data):
        _, word = data
        accepted_words.append(word)
        return data
    rule = Sequence([Raw(':'), Regex('\S+', callback=lambda x: x.group())],
        callback=callback)
    yield from parse(rule, source, position)

def known_word(source, position):
    rule = Choice([Raw(word) for word in accepted_words])
    yield from parse(rule, source, position)

line = Choice([new_word, known_word])
grammar = SeparatedStar(line, Regex(r'\s*'))
\end{verbatim}
\end{figure}
