\chapter{Evaluator}
\label{chap:evaluator}

In this chapter, we describe the structure of the evaluator in our interpreter
framework.  We discuss the built-in data types, the scoping environment
structure, and the evaluation strategy.

Our primary goal for defining evaluation was to balance the principle of least
surprise with existing programming language conventions.  Novice programmers
have little or no experience with concepts like scoping rules or function calls,
so we tried to simplify the rules that govern the evaluation of programs, when
possible.  However, there are some cases in which existing terms or conventions
are so strong that it would produce more confusion if we deviated from the norm.

We describe the built-in types and functions in \Cref{sec:built-ins}, the
environment model in \Cref{sec:environments}, and the evaluation strategies in
\Cref{sec:evaluator}.  Most of the built-in types and functions operate in
standard or straightforward ways.  The novel portion of the evaluator is the
creation of macro syntax rules, which are described in
\Cref{sec:make_syntax_rule}.

\section{Built-ins}
\label{sec:built-ins}

In this section, we describe the data structures supporting the built-in data
types and the functions used to operate on them.  These functions are included
in the base environment of the evaluator, so we refer to them as built-in
functions.

\subsection{Built-in Data Types}

We defined each built-in data type as its own Python class.  Each class inherits
from an abstract base class called \texttt{Root}.  In this section, we discuss
the primitive and compound types separately.

\subsubsection{Primitive Types}

The simpler literal values in the base language are implemented as primitive
types in the evaluator.  In this section, we discuss the implementation details
of these primitives.

\paragraph{Null and Booleans}
The base language implements three literals: a null type and the two booleans.
When checking value equality, each of these literals is only equal to itself.
We do not implement ``truthy'' or ``falsy'' checks for booleans, so \texttt{1}
and \texttt{True} are not considered equal in any context.

\paragraph{Numbers}
We chose to implement numbers as arbitrary precision rational numbers, which has
already been implemented by Python as the \texttt{fractions.Fraction} class.
Our numeric type (\texttt{Rational}) subclasses \texttt{Fraction}.  Novices
struggle with the some of the rounding issues that arise when floating-point
operations (the non-assocativity of addition, for example), so we chose not to
implement floating-point numbers.

\paragraph{Strings}
Our strings (implemented by the \texttt{String} class) store their delimiter in
order to display strings to the user the same way they were defined.  The
delimiter is only used for display.  String equality and other operations are
only concerned with the content of the string.

\subsubsection{Compound Types}

The remaining literals are implemented as compound types in the evaluator.  In
this section, we discuss the implementation details of these types.

\paragraph{Dictionaries}
A \texttt{Dictionary}, as presented to the user, is an immutable key-value
store.  The keys of a \texttt{Dictionary} can be arbitrary primitive or compound
types (including the \texttt{Dictionary} type) due to the immutability of all
language values.  Dictionaries are considered equal to each other if the
\texttt{frozenset}s of their key-value pairs are equal.  This allows us to say
that two dictionaries are equal if they print identically.

\paragraph{Functions}
We defined two classes for functions: \texttt{BuiltinFunc} and
\texttt{NativeFunc}.  \texttt{NativeFunc} is used to make user-defined
functions, and \texttt{BuiltinFunc} is used to wrap Python functions so they
satisfy the same calling interface as \texttt{NativeFunc} does.

\texttt{NativeFunc} stores its parameter names, the AST for its body, and a
reference to its parent environment.  At call time, it checks the number of
arguments passed, sets up the execution environment, runs the function body
block, and returns the value of that block.  If too many arguments are passed,
an error is raised.  All functions are anonymous, so there is no name to store.

If \texttt{partial\_application} is set in the settings file, passing fewer than
the required number of arguments to a \texttt{NativeFunc} performs partial
application, producing a new \texttt{NativeFunc} that takes that many fewer
arguments than the original one.

\subsection{Built-in Functions}

The following is a list of all functions included in the base environment of the
evaluator:

\begin{itemize}

\item \texttt{print(...)} is Python's \texttt{print} function.

\item \texttt{length(string)} returns the length of the content of a
  \texttt{String}.

\item \texttt{concatenate(string1, string2)} returns a \texttt{String} that is
  the concatenation of its two input \texttt{String}s, with \texttt{string1} as
  the prefix and \texttt{string2} as the suffix.

\item \texttt{characterAt(string, index)} returns a single-character
  \texttt{String} containing the character at the specified index in
  \texttt{string}. This follows common convention for indexing and starts at
  zero.

\item \texttt{contains(dictionary, key)} exposes Python's \texttt{in} operator
  for \texttt{Dictionary} objects.  It returns \texttt{Boolean(True)} when
  \texttt{key} exists as a key in \texttt{dictionary}.

\item \texttt{insert(dictionary, key, value)} returns a new \texttt{Dictionary}
  containing the same elements as \texttt{dictionary} except with
  \texttt{key:~value} added.  If \texttt{key} already exists in
  \texttt{dictionary}, the old mapping is replaced with the new one.

\item \texttt{remove(dictionary, key)} returns a new \texttt{Dictionary}
  containing the same elements as \texttt{dictionary} except with \texttt{key}
  (and its associated value) removed.  If \texttt{key} does not exist in
  \texttt{dictionary}, no error is raised, and the returned value will be equal
  to \texttt{dictionary}.

\item \texttt{keys(dictionary)} returns a \texttt{Dictionary} containing
  \texttt{index:~key} pairs for \\\texttt{dictionary}.  The indices start at zero,
  and the keys are in the same order that display to the user when
  \texttt{dictionary} is printed.

\item \texttt{size(dictionary)} returns the number of key-value pairs in
  \texttt{dictionary}.

\item \texttt{as\_decimal(number)} returns a \texttt{String} whose content is
  the decimal representation of \texttt{number}.  This is a way of converting
  the \texttt{Rational}s into their ``usual'' decimal notation, primarily
  intended as a convenient way to display the familiar representions of
  transcendental numbers or unfamiliar fractions.

\end{itemize}

\section{Environments}
\label{sec:environments}

The \texttt{Environment} class exists to keep track of stack frames and the
parent relationships between them for variable lookup.  A new environment is
created each time a function is called, which gives the language function scope
and lexical scope.  \texttt{Environment}s keep track of which bindings can be
changed.

\pagebreak

An \texttt{Environment} has the following instance attributes:
\begin{itemize}
\item \texttt{parent}: its parent \texttt{Environment} (or \texttt{None} if this
  is the base environment)
\item \texttt{settings}: a reference to the global \texttt{settings} object,
  which stores the settings loaded at initialization time
\item \texttt{variables}: a Python \texttt{dict} mapping names to their values
\item \texttt{constants}: a Python \texttt{set} of names with constant bindings
\end{itemize}

\paragraph{Name Resolution}
We implemented the \texttt{Environment}'s name lookup method as the
\texttt{\_\_getitem\_\_} magic method so we could use Python's bracket notation
to access variables.  Name resolution continues up the chain of parent
\texttt{Environment}s until the name is found in the \texttt{variables}
attribute of an \texttt{Environment}.  If resolution reaches the base
environment and the name has not been found, a \texttt{KeyError} is raised.

\paragraph{Constant Declaration}
The \texttt{declare\_constant(name, value)} method binds \texttt{name} to
\texttt{value} in the environment and marks \texttt{name} as constant.  If the
binding already exists, this raises a \texttt{RedeclarationError}.

\paragraph{Variable Declaration}
The \texttt{declare\_variable(name, value)} method binds \texttt{name} to
\texttt{value} in the environment.  If the binding already exists and the
setting \texttt{redeclaration\_is\_an\_error} has been set (or the existing
binding is a constant), this raises a \texttt{RedeclarationError}.

\paragraph{Assignment}
The \texttt{assign(name, value)} method is used to set the value of an
previously declared binding.  If \texttt{name} is bound in this environment, we
check that the binding is not constant and then set the value of that binding.
If the binding is constant, we raise an \texttt{ConstantAssignmentError}.

If the name is not bound in this environment and the setting
\texttt{declaration\_required} has not been set, we declare a variable binding
with that name and value in this environment.  Otherwise, we assume that a
declaration for this name has occurred in the parent environment chain and send
the assignment to this environment's parent instead.  If no \texttt{Environment}
in the chain finds a valid binding to assign, we raise an
\texttt{UndeclaredVariableError}.


\section{Evaluator}
\label{sec:evaluator}

The \texttt{Evaluator} class keeps track of the state of the interpreter
(macros, operators, and the grammar) and provides an \texttt{evaluate} method
that evaluates ASTs.  It also defines the base environment of the interpreter.

The \texttt{evaluate(ast, environment)} method returns the result of evaluating
\texttt{ast} in the context of \texttt{environment}.  In this section, we
describe the handling of each AST node type grouped by purpose.

\subsection{Standard Nodes}

In this section, we describe the evaluation strategies for the standard AST
nodes common to most languages.  These evaluate as expected by most experienced
programmers.

\paragraph{Expressions and Statements}
The \texttt{expr} and \texttt{statement} node types are wrappers around a single
expression or statement, respectively.

\paragraph{Numbers, Strings, and Literals}
A \texttt{number}, \texttt{True}, \texttt{False}, \texttt{Nothing}, or
\texttt{string} node evaluates to an object of the associated data type (as
described in \Cref{sec:built-ins}).

\paragraph{Dictionaries}
A \texttt{dictionary} node contains child nodes representing key-value pairs.
We evaluate each pair (key first, then value) and return a \texttt{Dictionary}
containing these pairs.

An \texttt{access} node (representing a dictionary lookup expression such as
\texttt{d[key]}) contains two child nodes: a dictionary and a key.  We evaluate
the dictionary node and the key node and then return the corresponding value for
that key in the dictionary.

\paragraph{Operators}
A \texttt{binary\_operation} or \texttt{unary\_operation} node contains child
nodes representing the operator symbol and the operands.  We look up the
evaluation function associated with the operator, evaluate the operand nodes,
apply the evaluation function to the operand values, and return the result.

\paragraph{Blocks}
A \texttt{block} node contains a sequence of \texttt{statement} nodes.  We
evaluate each statement, in order, and return the value of the last statement
evaluated.  If the block is empty, we return \texttt{Nothing}.

\paragraph{Functions}
A \texttt{function} node represents a function definition and contains the
parameter list and body of the function.  We create a \texttt{NativeFunc} with
those properties, setting the function's parent environment to be the current
environment.

A \texttt{call} node (representing a function call such as \texttt{f(x, y)})
contains child nodes representing a function and an argument list.  We evaluate
the function node and each of the argument nodes, in order.  We then call the
function on the arguments and return the result.

\paragraph{Variables}
An \texttt{identifier} node signifies a variable lookup and contains a
variable name.  We return the result of looking up that name in the current
environment.

An \texttt{assignment}, \texttt{declare\_variable}, or
\texttt{declare\_constant} node contains an identifier and a value node, so we
evaluate the value node and perform the appropriate action with the name and
value.  For the special case in which the identifier is actually an
\texttt{unquote} node and therefore represents a metavariable (see below), we
evaluate the unquote node to get a variable identifier before performing the
action.

\paragraph{Control Flow}
An \texttt{if} node contains nodes for its condition, consequent, and
alternative blocks.  We evaluate the condition node to determine which branch to
take, evaluate the correct branch, and return the results of evaluating that
branch.

A \texttt{while} node contains the nodes for its condition and the body.  We
begin by evaluating the condition node.  If the condition evaluates to
\texttt{True}, we continue by evaluating the body.  We then continue from the
condition node, looping this process until the condition evaluates to
\texttt{False}.  The overall return value is the value returned by the last
evaluation of the body.

\subsection{Macro Nodes}

In this section, we describe the evaluation strategies for the AST nodes
involved in the macro system.

\paragraph{Macro Definitions}
The \texttt{macro\_block} and \texttt{macro\_expr} nodes are used to define new
block macros or expression macros, respectively.  Each of these nodes contains
the name of the macro, any metavariables to be declared, the syntax description,
and the macro body.  When evaluating a macro node, we create the appropriate
parser rule from the syntax description and add it to the grammar by appending
it to the grammar's \texttt{statement\_types} or \texttt{expr\_types} list.

\paragraph{Macro Expansions}
A \texttt{macro\_expansion} node denotes the expansion of a previously defined
macro rule.  We look up the macro name in the state of the evaluator, generate
names for each of the metavariables, and update the current environment to
contain the mapping of metavariable names to variable names.  We then evaluate
the body of the macro in the current environment and return the result.

\paragraph{Unquote}
An \texttt{unquote} node indicates the expansion of an expression containing
metavariables.  In this case, we evaluate the unquote node to obtain the
corresponding identifier node and then evaluate that identifier node to obtain
the associated value.


\section{Creating Macro Rules}
\label{sec:make_syntax_rule}

When evaluating a \texttt{macro\_block} or \texttt{macro\_expr} node, we create
a new grammar rule describing the syntax that this macro should match.  We use a
function called \texttt{make\_syntax\_rule} to accomplish this.  The callback
functions for these rules are nontrivial, so we discuss them here.

\subsection{Macro Syntax Rules}

\texttt{make\_syntax\_rule} takes four arguments: the syntax rule specification,
the node type to capture, a reference to the whole grammar, and a flag (called
\texttt{multi}, for ``multiple match'') that indicates whether the rule is part
of a repeated syntax specification.  The data attribute of a \texttt{Match}
emitted by this macro rule will contain a Python dictionary that maps
metavariable names to the captured ASTs associated with each.  In the case of
repeated syntax, the value in the dictionary will be a list containing each AST
that was captured, in order.

There are four types of syntax patterns that can be matched by the syntax
specification of a macro (described in \Cref{sec:macro-mini-language}):

\paragraph{Sequence}
The syntax pattern for an entire macro is always a \texttt{Sequence} rule whose
terms are the individual syntax patterns.  These sub-patterns are created
recursively, using \texttt{make\_syntax\_rule}.

\paragraph{Metavariable}
A metavariable rule is denoted by an identifier in the syntax specification.  A
metavariable matches one or more expressions or blocks, depending on whether
this is part of a repeated rule and which type of macro is being defined.  We
can match an expression or block using the \texttt{expr} or \texttt{block} rule
from the grammar, but we use the \texttt{Single} combinator to add a second
callback for post-processing the captured AST.

\paragraph{Keyword}
A keyword rule is denoted by a string in the syntax specification.  We create a
new \texttt{Raw} rule matching the string exactly, and set its callback to
always return an empty metavariable mapping (to simplify combining them).  We
also add this keyword to the set of reserved words in the grammar.  This means
that it is possible to make an existing variable name impossible to use by
including it as a macro keyword.

\paragraph{Repeat}
A repeat rule is denoted by a repetition match in the syntax specification.  We
use the cardinality of the repetition to select among the \texttt{Optional},
\texttt{SeparatedPlus}, and \texttt{SeparatedStar} combinators and then make the
appropriate rule using the repeated rule (made with \texttt{make\_syntax\_rule})
and separator rule (made with \texttt{Raw}).

\subsection{Callback Functions}

The callback functions for rules created by \texttt{make\_syntax\_rule} exist to
collect metavariable mappings to captured ASTs.  The metavariable and keyword
rules define partial mappings, and the sequence and repeat rules combine them
into complete mappings.  The callback function for the entire macro rule
converts this mapping into a combined AST node that can be passed to the
\texttt{evaluate} method.

\paragraph{Metavariable}
The callback function for a metavariable rule takes the captured AST (an
\texttt{expr} or \texttt{block} node) and returns the partial mapping of the
metavariable name to this node.  The multi flag indicates whether this
metavariable is part of a repeated match.  In the case that \texttt{multi} is
not set, the mapping is simply \texttt{\{metavariable: AST\}}.  In the case that
\texttt{multi} is set, the mapping is \texttt{\{metavariable: [AST]\}}, since
repeated matchings result in lists of ASTs.

\paragraph{Keyword}
The callback function for a keyword rule always returns the empty mapping, since
keyword cannot create a metavariable bindings.

\paragraph{Repeat}
The callback function for a repeat rule depends on the cardinality of the
repetition.  For the zero-or-one cardinality (\texttt{?}, \texttt{Optional}),
the callback is either passed \texttt{None} (indicating no match) or a partial
metavariable mapping (from an earlier callback function).  In the case of
\texttt{None}, we return an empty mapping.  In the case of a partial mapping, we
return that mapping.

For the one-or-more (\texttt{+}, \texttt{SeparatedPlus}) and zero-or-more
(\texttt{*}, \texttt{SeparatedStar}) cardinalities, the callback function is
passed a list of partial mappings.  We combine those into a single mapping by
concatenating the lists for repeated metavariable bindings and overwriting the
existing bindings for single metavariable bindings.

\paragraph{Sequence}
The callback function for a sequence rule is the same as for a repeat rule.  We
combine the partial bindings for each of the terms, handling repeated matches
separately from single matches.

\paragraph{Macro}
The callback function for the entire macro rule converts the final metavariable
mapping (from the sequence rule) into a single \texttt{macro\_expansion} node.
This node contains two children: the name of the macro, and the macro parameters
in a form that will work with the unquote operator.

We expose the individual ASTs captured by a repeated match in the form of a
special dictionary object (\texttt{LazyDictionary}).  Using a normal
\texttt{Dictionary} for this purpose, would cause every captured AST to be
immediately executed at expansion time, which gives macros the same semantics as
functions.  We wanted the user to control the evaluation of captured ASTs within
the body of the macro, so a \texttt{LazyDictionary} delays the evaluation of its
values until they are accessed.  It does not cache the results (so the
\texttt{Lazy} prefix is a bit of a misnomer) becasue caching in this way would
make it impossible to evaluate an AST for its side effects more than once.


\subsection{Example}

In this example, we describe the process of interpreting a program that defines
the \texttt{list} macro.  The source of this program is in
\Cref{fig:list-source}.

The syntax specification of this macro consists of a keyword, a repeated rule,
and another keyword.  When evaluating this definition, the strings \texttt{'['}
  and \texttt{']'} are added to the \texttt{reserved\_substrings} set in the
grammar.  The syntax rule added to the grammar (at the front of the
\texttt{expr\_types} list) is equivalent to the rule shown in
\Cref{fig:list-rule}.  At expansion time, the body of the macro will run in an
environment with the extra variable bindings shown in \Cref{fig:list-bindings}.


\begin{figure}[htbp]
\caption{An equivalent syntax rule for \texttt{list}}
\label{fig:list-rule}
\begin{verbatim}
Sequence([Raw('['), SeparatedStar(expr, Raw(',')) Raw(']')])
\end{verbatim}
\end{figure}

\begin{figure}[htbp]
\caption{Extra variable bindings for \texttt{list}}
\label{fig:list-bindings}
\begin{verbatim}
values: (identifier '$macrosym_0')
i: (identifier '$macrosym_1')
l: (identifier '$macrosym_2')
$macrosym_0: (LazyDictionary
               ((integer 0) (expr (string 'Hello' "'")))
               ((integer 0) (expr (integer 2)))
             )
\end{verbatim}
\end{figure}
