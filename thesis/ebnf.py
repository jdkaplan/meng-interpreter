import string
letter = Choice([Raw(l) for l in string.ascii_letters])
digit = Choice([Raw(d) for d in string.ascii_digits])
symbol = Choice([Raw(s) for s in '''[]{}()<>'"=|.,;'''])
character = Choice([letter, digit, symbol, Raw('_')])
terminal = Choice([
        Sequence([Raw("'"), Plus(character), Raw("'")]),
        Sequence([Raw('"'), Plus(character), Raw('"')]),
])
lhs = identifier
rhs = Choice([
        identifier,
        terminal,
        Sequence([Raw('['), rhs, Raw(']')]),
        Sequence([Raw('{'), rhs, Raw('}')]),
        Sequence([Raw('('), rhs, Raw(')')]),
        Sequence([rhs, Raw('|'), rhs]),
        Sequence([rhs, Raw(','), rhs]),
])
rule = Sequence([lhs, Raw('='), rhs, Raw(';')])
ebnf = Star(rule)
