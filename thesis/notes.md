The Problem
===========

programming language designed for teaching

"for teaching"?  As a pedagogical tool to teach programming basics and patterns

An environment where students can easily explore the consequences of their
modifications to existing programs

Creating skeleton code that gets students started

Minimizing the need for skeleton code

Setting up structures for the students to use, and slowly removing those
structures so the students learn to build them by themselves when needed

Hide complicated language features at the beginning so students can get used to
using the analysis techniques and mental models that the instructor wants to use

Avoid IEEE 754 floating point forever (or at least until it can be explained
"properly")

A language whose syntax can be modified by the user to allow for easy expression
of new patterns they discover in their work

Extensibility in the interpreter to allow for language features I forgot about

Separate syntax from programming by providing lots of freedom in the grammar

Allow for the existence of different syntax constructs for the same idea
(e.g. verbose / quick declaration)


Introduction
============

cite something about how "function () ... end" is better than "function () { ... }"

The base interpreter starts with rational numbers, hash-table dictionaries,
if-expressions, while-expressions, functions, and macros.

Assignment/declaration can be easily added to the interpreter, but people argue
over that syntax, so I left it up to whoever's using it to choose what they
wanted.

Anything that isn't a reserved substring can be used as an identifier.  Once
macros show up, this can cause problems, but macros are already a great way to
shoot yourself in the foot.  Problems can be avoided by knowing the rules for
the macros.

maybe do error reporting on possible name conflicts at macro definition time?

The style I took for macros was that they were exclusively runtime macros that
placed little restriction on the syntax of the macro.  To achieve that, I had to
be able to change the grammar at runtime during the evaluation phase, which
meant that the parser had to be modifiable during the parse of the program.  I
couldn't find any parser generator that would allow me to do this, so I wrote my
own parser generator and set it up to incrementally parse one statement at a
time and wait until it was evaluated (potentially modifying the parser) before
continuing to parse.


generator
=========

The parser has a few base elements: `raw`, `regex`.  The combinators are
`sequence`, `choice`, `separated_star`, `separated_plus`, `optional`, `star`,
`plus`, and `single`.  Really, any Python function can be a parser, as long as
it accepts the source string and a position index and yields Match objects that
it considers matched at that index.  `identifier` is an example of a weird
parser like this.

The custom parsers not made my the combinators have a lot of freedom in what
they can do.  There's arbitrary lookahead and lookbehind (although I never use
it), they can be stateless or stateful, and they can put anything they want as
the data portion of the Match object.

I use the data field to build up the AST that will eventually be parsed.

The parsers all get wrapped in something that generates the infinite sequence of
None objects when the original parser runs out, and the combinators make use of
a takewhile(not None, ...) to get whatever matches exist.  I don't know if this
is actually necessary.

Parsers produces `None`s when they stop matching, and that's what the
combinators do once they've found an inability to match.  The reason for the
streaming is that something like a sequence of choices has to be able to try the
third option in the first choice before calling the whole thing a failure
because the last choice in the sequence had no matches when the second match
from the first choice was used.

`choice` is ordered!

The reason I even had to make this generator is because none of the parser
generators I tried (PLY, grako, Arpeggio, etc.) would let me get a partial parse
of a string.  None of them allowed for adding rules at runtime, but that could
have been remedied by rebuilding the parser every time a macro was defined, as
slow as that would be.

This is a recursive descent parser with backtracking.


grammar
=======

The grammar relies on a few basic constructs being available: keywords,
literals, reserved_words, and reserved_substrings.  Each of those describe
exceptions to the everything-is-a-valid-identifier rule.  `keywords`,
`literals`, and `reserved_words` are simple filters for names that can't be used
in their entirety.  'True' can be part of a name, for example (returnTrue), but
`True = False` is an invalid assignment.  `reserved_substrings` describes
substrings that cannot appear anywhere in identifiers.  This is combined with
the set of operator strings to disallow 'a+b' as an identifier in favor of
parsing it as the more-likely-intended addition of a and b.

`expr_types` is a list of expression parsers in the order that they should be
matched.  When an expression macro is defined, it gets inserted at the beginning
of this list.  The only type of expr not contained in this list is a unop or
binop, because those are compound expressions and therefore handled differently.

TODO: cite the source of the precedence climbing algorithm, and discuss how it
was adapted for the streaming parsers.

expr_suffix is a hack for getting the correct precedence of calls and accesses.
It forces them to be considered early and allows them to be chained without
making them part of the basic expr group.  (THERE'S A BUG!)

The parser defines a mini-language for defining macros, which borrows heavily
from EBNF.  Some of its weirdness (syntax, transform) isn't strictly necessary,
but I couldn't think of any way of making those parts explicit to readers
without using keywords.

`statement_types` is a holdover from when not everything returned a value, but
now its only purpose is to not allow macro definitions to be used as the
right-hand sides of assignments.  It also prevents macro_block rules from being
the RHS of an assignment.

Blocks are newline-separated sequences of statements, so every statement must be
on its own line (unless it contains a block, but that's handled separately by
other individual grammar rules).

Whitespace is not important other than newlines as a statement delimiter, and
any non-line-breaking whitespace as a delimiter for other tokens.

The grammar_post_create hook runs on the grammar once all the basic definitions
are done, which allows for any modification of the grammar.  This is used by my
settings files to define assignment syntax rules.

TODO: can rules be overwritten in post_create?  ex: rebind statement, which is
used by block

The entire namespace internal to the function Grammar is returned as the
grammar, so any individual rule can be parsed explicitly by any user of the
rammar, namely the interpreter.


__main__
========

The settings files passed to the interpreter define the different behaviors of
the interpreters that this structure can create.  The settings are combined
(TODO: util.get_settings) and passed into the grammar creation and evaluator
creation.

There is no import mechanism, but a file can be sourced into the current
environment by including it in the list of files to be run.  The files are run
in the order specified.  If no files are specified, it starts a REPL, but there
is a flag to enter the REPL once all the files have finished running.

The REPL is very rudimentary.  It can't handle multi-line statements, and should
mainly be used as a quick calculation tool or for very simple programs.  Using
the interpreter interactively can help with examining the state of data
structures at the end of a program, but I recommend always using the interpreter
with script files.


interpreter
===========

This is the real place we want to be able to parse arbitrary grammar rules.  The
strategy here is to loop through skipping whitespace, finding a statement, and
evaluating that statement.  This also the place where the prefix parse is
necessary, because the statement we find might be a macro, in which case we have
to evaluate it and add its rule to the parser before we can continue parsing for
new statements.

The error message on a syntax error is terrible.  I think that it could be made
better by improving the parser generator to yield an error message instead of
`None` when a parsing error occurs.


evaluator
=========

The evaluator shows that this interpreter may have been better off if it had
been written in a language with tail call optimization.  It takes a very long
time to do some reasonably simple things (euler/001.prog), and I'm pretty sure
it's the lack of TCO.  (time it!)

The `Environment` class is made to keep track of namespaces and the parent
relationships for function scopes.  A new environment is created for each
function call evaluation, which gives the language lexically-scoped variables.
Environments keep track of which variables are defined as immutable and produce
errors when immutables are re-assigned or when a variable is re-declared.


Built-in types
--------------

`Nothing` is the default null-like type.

`Boolean` is the default boolean type.  It could be split into separate TrueType
and FalseType, but this was easier when implementing `lift`.

`Rational` is the default numeric type.  This allows `/` to only ever mean
non-truncating division, and makes the majority of quick calculations easy to
represent.  Transcendentals like `pi` and `sqrt(2)` now show up in unusual forms
(fractions instead of decimals) if they're going to keep the same precision that
most students have become used to.  There could be a lot of value in integrating
true symbolic math with a package like SymPy, but we haven't tried that yet.

`String` is the default string type.  It carries around the delimiter it was
defined with in order to display the same way it was expressed.  Everything
except display depends on the content only.

`Func` is a simple parent class for functions.

`NativeFunc` is the class for real functions in the language.  It stores its
parameter names, the AST for its body, and the reference to its parent
environment.  At call time, it checks the number of arguments passed, sets up
the execution environment, runs the function body block, and returns the value
of that block.  If `partial_application` is set, having fewer than the required
number of arguments produces a new function that takes fewer arguments with a
new parent environment that has the provided arguments already set as
parameters.  The intermediary environment (with the provided arguments) is
created instead of immediately assigning the parameters in order to allow the
function to re-assign bound parameter names without worrying about their reuse.
With any settings, if too many aruments are passed, an error is raised.  All
functions are anonymous, so there is no name to store.

I *have* thought about JavaScript-style named function expressions to allow for
self-recursive anonymous functions, but it hasn't been implemented yet.
Alternatively, someone could try explaining the Y combinator to novice
programmers and pose it as an exercise to turn `almost-fib` into `fib`.

`Dictionary` is the class for unordered collections, which are the only compound
data type by default.  One way to create an ordered data type would be to create
a Dictionary with only contiguous integer keys, and all its usage would be
identical to the `List` type that is not included.  The decision not to include
`List` was primarily made to reduce the number of basic data types in the
language as a means of letting students build their own abstractions on top of
them.  For example, many people disagree on whether indexing should start at 0
or 1 [citation needed].  This avoids the problem entirely by letting people
(forcing them to, in fact) decide where to start from.  In some domains, such as
linear algebra, numbering naturally starts at 1.  Most other programming
languages start at 0, but I'm not going to force that on anyone with an opinion
that differs from mine.  Dictionaries print in key-sorted order, which first
sorts on type (Nothing, Boolean, Rational, String, BuiltinFunc, NativeFunc,
Dictionary), and then on value.

After writing all this, I realized that I actually did make some indexing
decisions.  I made the following all start at zero: string indexing, the `keys`
function on dictionaries, and macro binding dictionary indexing.  I guess what I
really meant here is that I don't want to force the indexing of the List or
Array or Matrix types when they're defined by the user.  I wouldn't want someone
stuck with zero-indexed matrices or one-indexed Arrays.


Built-in functions
==================

`print` is Python's print statement exposed to the user.  This is probably why I
gave everything a `__repr__` and `__str__`, even though I never call them
myself.

`length` returns the length of the content of the string.

`concatenate` returns a String that is the concatenation of its two input
Strings.

`characterAt` returns a single-character String containing the character at the
specified index in the provided String.  This follows Python indexing and starts
at zero.

`contains` exposes Python's `in` operator for Dictionary objects.  It returns
`True` when the key exists in the dictionary.

`insert` returns a new Dictionary containing the same elements as the provided
one except with `key: val` added.  If key already exists, the old mapping is
removed in favor of the new one.

`remove` returns a new Dictionary containing the same elements as the provided
one except with `key` removed.  If `key` does not exist in the dictionary, there
is no error, and the returned value will be the same as the old dictionary.

`keys` returns a Dictionary containing `index: key` pairs for the provided
dictionary.  The indices start at zero, and the keys are in dictionary-sorting
order.

`size` returns the number of `key: value` pairs in the dictionary.

`as_decimal` is a way of converting the `Rational`s into their "usual" decimal
notation, mostly for the ease of representing transcendentals.  In the future, a
flag could be added that changes the default representation of numbers from
fractions to decimals.


`lift`
------

`lift` is a hack for making sure that `Evaluator.evaluate` can never return a
type that isn't a subclass of `Root`.  This is called everywhere that something
returns to the user out of the interpreter, and was my main source of error
checking types that left the evaluator.  It made sense to centralize the mapping
from Python types while it was still in development.  In a "real" interpreter,
this would have been type-checked by the compiler, but I chose Python for ease
of prototyping.  This can be mitigated by wrapping only the times that I
actually make Python do things, but I'd have a hard time finding them now.


Evaluator
---------

The `Evaluator` class keeps track of the state of the interpreter (macros,
operators, and the current grammar) and provides an `evaluate` method that
actually runs programs.  `evaluate()` is essentially a giant switch statement
based on the AST node's type.  'expr' and 'statement' nodes are just containers
for other nodes, so they get immediately unpacked.  'block' nodes require
evaluating each of the statements in order and returning the value of the last
statement with the empty block returning `Nothing`.  'number', 'True', 'False',
'Nothing', 'string', and 'dictionary' all just create the value they're asked
to.  'access' gets a value out of the dictionary.  ... <!-- ??: Do I really need
to describe this in words?  Can I just make a table of AST types to a four-word
phrase? -->


Normal AST node types:
* expr
* statement
* number
* True
* False
* Nothing
* string
* dictionary
* access
* function
* call
* identifier
* assignment
* declare_mutable
* declare_immutable
* if
* while

Weird AST node types:
* binary_operation
* unary_operation
* macro_block
* macro_expr
* unquote
* lazy_dictionary


`make_macro` handles making both kinds of macros (expr and block).  It gets
called during the evaluation of a macro definition.  It produces the name of the
macro to be defined, the syntax rule to be added to the grammar (via
`make_syntax_rule`), and a `Macro` object (which is just a container for the
names of all the meta-variables and the AST of the macro body).

Arguments to `make_macro`:
* ast: the ast of the macro.  Used to get the name, metavariables, syntax, and body
* node_type: the grammar rule used to match sub-expressions or sub-blocks (??: what's the word for this?)
* supernode_name: the AST type that the generated syntax rule should emit (always 'macro_*_expansion')
* grammar: a reference to the entire grammar (for use in `make_syntax_rule`)

The `callback` defined in `make_macro` takes the matches found by the generated
`rule` from `make_syntax_rule` and collects them into a proper evaluate-able
AST.  The matches are collected my the parser rules into a dictionary that maps
the metavariable names to their captured ASTs.  If a value in that dictionary is
a list, that means that the macro requested (potentially) repeated matches.  In
that case, those matches are re-collected into a `lazy_dictionary` AST.

<!-- TODO: good place to put macro examples -->

`make_syntax_rule` was only broken out of make_macro because I wanted to be able
to test the parser rules I was making.  It's only real use is to make the macro
parser rule to be added to the grammar.  It takes the syntax rule chunk of the
macro AST, the node_type to capture, a reference to the whole grammar, and a
flag to allow multiple matches.

There are four kinds of rules made in `make_syntax_rule`:

1. expr and block: Which rule depends on the type of macro, but these are
   matched when the macro syntax specifies a metavariable to bind a match to.
   The data produces here is a mapping of the metavariable name to the matched
   AST.  When `multi` is set, the name maps to a singelton list containing the
   matched AST to aid in merging the multiple matches.

2. raw string: This is made when a keyword is included in the macro rule.  This
   produces a `generator.raw` that always has an empty mapping as its data.  The
   word being matched is added to the set of reserved words in the grammar at
   the same time that this rule is created.  It might be worth collecting those
   later in case of a failure in rule-creation, but that's a problem for later.
   If the user isn't careful, they can make one of their existing variable names
   un-spellable by including it as a keyword in a macro.  We do nothing to
   prevent this.

3. multi-match rules: These are made when the user specifies a repeated match.
   The three parts of that multi-match (the repeated rule, the repetition count,
   and the separator) are used to create the correct `generator.optional` or
   `generator.separated_{star,plus}` rule that matches the correct number of
   times and creates the correct meta-variable mapping from the sequences.

4. sequence rule: This is the default sequence rule matching that the syntax
   specification makes.  This is a `generator.sequence` that matches the rules
   created recursively from each of the component syntax pieces.


Implementation details
----------------------

The `Root` class is simply there to be a superclass for built-in objects.  It's
used for `isinstance` testing and not much else.  It could grow into a true
abstract class definition, removing the reliance on Python's magic methods, but
that can be left for a future version.

`BuiltinFunc` is a simple wrapper for Python functions to satisfy the `Func`
interface.  This should probably be renamed to `PythonFunc` because there's
really nothing "built-in" about it.  It mainly exists to make `lift` work.

`LazyDictionary` is a hack to delay the evaluation of macro-bound ASTs until
they were properly referenced inside the macro.  Most things labeled "lazy"
would cache their value after being evaluated, but this does not, so its name is
a bit of a misnomer.  This is not a user-facing type, and it is never exposed
outside of the `evaluator.py`.

`__lt__` being used to define total ordering is a hack that should be removed.
Dictionaries have no reason to print sorted, even though I'd like them to.  This
should either be moved into a special method (just for Dictionary sorting) or
deleted.


settings
========

partial_application: When set, this allows a function to be called
with an insufficient number of arguments, which will return a new
function with those arguments partially applied.

redeclaration_is_an_error: When set, this will fail when declaring
multiple variables with the same name in the same scope.  This
probably causes functions defined with many of the same throwaway
parameter name to fail at call time, so it may not always be a good
idea to have this set.

canonical_string_delimiter: Some people want all their strings to have
the same delimiters.  When set, this string is used as the delimiter
when displaying strings, regardless of how they were defined.

immutable_function_arguments: If set, function arguments are declared
in the function body's scope as immutable.  By default, they are
declared as mutable.

unquote_operator: The string to use for the unquote operator.  I like
'@', but Lispers may prefer '`'.  If you like verbosity, try
'unquote'.

binary_operations and unary_operations: These can be used to specify
the operators that will exist by default.  Each operation is specified
as a precedence level, an operator symbol, (an associativity, in the
case of binop), and an evaluation function.  This is mostly meant as a
mechanism to provide math and logic operators with whatever symbols
are practical for the environment.  For example, some people prefer
having '^' mean exponentiation (instead of '**'), especially when
binary XOR doesn't exist.

grammar_post_create: This is a list of functions to run on the grammar
after it's defined but before anything starts parsing.  I mainly use
this to add asignment and declaration rules to the syntax, because
they're not there by default.  Assignment was where I had the most
trouble deciding between syntax, so I pushed that decision off to
someone else.  Since this can do anything it wants to (TODO: check if
a rule can be killed), any other syntax properties can be defined or
modified here.  Examples: js_like.py, basic.py, strict.py

There's no such thing as evaluator_post_create because I never saw a
need for one, but that could definitely be added.  When combined with
grammar_post_create, it would probably be able to fulfill any other
syntax/evaluation needs.


macros
======

Macros are nothing new.  Every good Lisp has them, C has them, and modern
languages are getting them now too.  I think that macros get a lot undeserved
hate, mainly from programmers who have used C preprocessor macros or programmers
who have struggled with making their Lisp macros hygienic.

I can't decide if I see macros as part of the same layer of abstraction as
functions or one layer above them.  Functions can be used to abstract away
computations done on values.  Macros are used to abstract the patterns of
computation done on syntax elements.

Text-replacement macros (like those in the C preprocessor) are fragile in the
sense that their use can cause unintended consequences for program parses. <!--
TODO: insert the usual examples -->

Expression-replacement macros usually rely on homoiconic syntax, or at least the
ability to easily see what data structures your macro parse would capture. This
language doesn't have the property that the program text for an expression is
literally its syntax tree, so macros couldn't be exposed to users as
s-expression transformers.  Nor would we want them to be.  This language is not
homoiconic, it has infix operators and operator precedence, and the AST
generated is difficult to deduce unless you wrote the parser.

These macros are slightly less powerful than Lisp macros because there's no
`quote`, no symbols, and no way to test equality between ASTs.  Most macros I've
encountered have been created to capture fairly regular patterns in syntax, so
capturing the common case was part of the goal for a novice-oriented macro
system.  The system had to produce automatically-hygienic macros, because macro
hygiene is not interesting or important for novice programmers.

Because this language doesn't have uniform syntax like Lisps do, part of making
the macro system involved having the ability to modify the parser at runtime.
There are no guarantees that the entire program text can be parsed because macro
definitions in the middle of a program can add rules that the rest of the
program depends on.  The recursive descent parser is structured in a way that
allows its data structures to be modified in between the parsing of two
statements.  The evaluator, when evaluating a macro definition, is able to
create these rules and add them to the parser before it is asked for the next
statement.  It is impossible to compile programs in this language because of the
macro system.

<!-- TODO: `swap a b` macro example -->
<!-- TODO: do/should macros scope? -->


notebook
========

Seven Deadly Sins
-----------------

I implemented as few data structures as I could get away with, but I tried not
to make the syntax so homogeneous that it needs so many special functions.  The
base grammar avoids syntactic homonyms, both for the user's sake and the
parser's.  There is almost nothing implicit semantically except for the macro
definitions and special settings-defined operators.  I purposefully included
some backward (forward?) compatibility to other languages, mainly in the form of
syntactic memes.  However, nothing about the language prevents using × as
multiplication operator (for example) except for the inability to type it on a
standard keyboard.

Maybe I should implement `break` as `exit loop` but allow for named loops,
because of the likelihood of wanting to terminate all parent loops.  It seems
like this paper advocated more for *only* having break, e.g.:
```
x = 10
loop
    print(x)

    if x <= 0 then
        exit loop
    end if

    x = x - 1
end loop
print("BOOM!")
```

There's no I in IO!  I never implemented read.  A teacher could just expose
Python's `input()` as whatever name they want, though.  I also forgot about file
IO.  `x == eval(input(to_string(x)))` should be true for all `x`.

Error messages are probably the worst I've ever seen.  This is definitely a
place for future work.  This can probably be done by augmenting the AST
structures with the line+column numbers of their components and updating the
interpreter and evaluator pieces to display meaningful errors.  For parse
errors, the parser generator can be extended to yield a ParseError type that
contains an error message.  It would be pretty cool to see `choice` display the
failure that each parser had, while `sequence` can display the error from the
first failed parser.

Reading this paper the first time, I essentially re-invented Racket's `#lang`
mechanism, but it looks like I wanted it to be repeated the way that `import`
statements usually are.  An example of such a program header:

```
use integers
use {
    +: add,
    *: times,
    -: minus,
    /: divide,
    %: modulo,
} from math.binary
use {-: negate} from math.unary
use list, for from macros
```

It should be possible to define a suffix operator `%` that actually converts
numbers into percentages.  I don't know if my implementation of precedence
climbing does that, but I know my settings definitions don't.  However,
`grammar_post_create` definitely does!

Tweaking the grammar to use `end while` and `end function` instead of just `end`
is not very hard at all.


Random notes
------------

Features that were intentionally not implemented:
* Ruby/JS/shell interpolated strings
* Python raw strings
* separate multiline string syntax
* literal regexes

Features that I forgot to implement:
* Paired delimiters on strings (e.g. `$(this is a string!)` and `$<paired quotes>`)


Building Interpreters by Composing Monads
-----------------------------------------

This is definitely the next place to go.  It'll allow for the cool `use ...`
lines above, and make it easier for instructors to add new features to the
interpreter.  It also allows students to choose which versions of features they
want.  For example, block vs. function scope or call-by-value vs. call-by-name
function calls.

I had originally tried this implementation of an interpreter, but it became
unwieldy when goto came around.  It was also substantially different in
implementation because of Python vs. Haskell.  It also had some performance
issues because of Python's lack of tail-call optimization (or at least the
reference CPython implementation).  Evaluating any expression required
traversing the interpreter stack until a suitable module was found to evaluate
it.  This meant that an increasing depth of module and an increasing depth of
the syntax tree both contributed to runtime.  That could probably also be
handled differently, (possible `while True:`ing like we do now).


Random notes
------------

Because we're using rational numbers as our default numeric type, there should
be a rule for it in the grammar.  Most languages use `/` for division, so I just
let `1/2` parse as `(binop / (integer 1) (integer 2))` instead of `(rational 1/2)`.
If someone really wanted rationals without division, it's only small tweaks to
the grammar and evaluator.

At some point we considered compiling to run on the Python VM or the JVM.  The
Python VM was intentionally not specified, and I had some doubts about this
interpretation strategy not working on the JVM.  If the JVM can support Clojure,
it can definitely support this (in runtime, at least), so I have no idea what I
was thinking.

My first attempt at a parser was to write a shift/reduce by hand, but I failed
in every conceivable way (at the design phase, too!).

Macros were modeled after Racket's `define-syntax` and `defmacro`, in which the
user specifies an AST transformation function.  I wondered what to do about the
`symbol` datatype, and noted that I would have to provide one of `if`, `cond`,
or `switch` (or use `call-by-need/name`) to be able to get away with not
implementing functions.


Base Features v1
----------------

* integer
* identifier
* assignment
* if-then-else
* anonymous function
* function call
* list literal
* list index
* dictionary literal
* dictionary lookup
* list assignment to index (`lst[idx] = val`)
* dictionary assignment to key (`dct[key] = val`)
* strings (`/\$(\S)[^\1]*\1/`)


PLY
---

I started the parser as a PLY parser, happy to hack my grammar so as not to
include infinite left recursion.

I figured we could just make lists out of macros around dictionaries.

I thought about various syntaxes for loops:
* `LOOP setup condition REPEAT body END`
* `setup WHEN condition DO body LOOP`
* `setup REPEAT condition DO body END`
* `setup REPEAT body IF condition END`
* `setup REPEAT body WHILE condition END`

I didn't want to choose between them, so we decided that `goto` might be the
right way.  (I have a note that shows that I was not happy about this
discovery.)  Wary of the problems that `goto` brings, I refused to let it be
C-style `goto` and forced it to be scoped.  A label could not be jumped to
unless it was within the current scope, hopefully avoiding the problems of
jumping into someone else's function unintentionally.

The grammar gained the idea of a statement (as opposed to just an expression)
which allowed for the control of program state.  Assignment, if, label, and goto
were considered statements, and expressions were statements just to make parsing
work (and because function calls contained statements).  `loop` was removed in
favor of `goto`, and `break` and `continue` were then made unnecessary.


Random notes
------------

I think I have every PLY grammar I ever tried written down in this notebook.

I noted that there were differences in the connotations of require, include,
import, and source.  I also wondered if it was worth having multiple modules or
if just multiple files would be enough.


Macros
------

```
macro all (L)
    if L[0]:
        all(L[1:])
    else:
        False
```

The goal for macros was to get a new unary operator, a new binary operator,
while/for loops, and list literals.

Examples:
* `~True` -> `False`
* `A \cdot B` -> `dot_product(A, B)`
* `[1, 2, 4]` -> `{0: 1, 1: 2, 2: 4}`
* `while c do b` -> ```
    loop:
        if c then
            b
            goto loop
        end
```

Parsing this is impossible unless we severely restrict macro usage.  (what?)

    syntax unary_operator ~ is
        function(arg) not(arg) end
    end

    syntax binary_operator \cdot is
        function(left, right) ... end
    end

New operators are easy because they can be added to the parser ahead of time.
Can we do the same to real syntax safely?  Can we modify existing syntax?
(e.g., `f(*args)` -> `print(f, args); f(args)`)  I want pattern-matching.


Operators
---------

Problem: By the time we would want to evaluate an expression containing a custom
operator, the source has already been parsed with the operator as an identifier.
We need to re-define and re-build the lexer+parser mid-evaluation.  We also need
to re-parse the remaining source, in case it uses the operator we're defining.
But what about nested operator definitions?  What if some function uses a custom
operator before it's defined?

Actually, in the case of co-recursive operators, this isn't a problem.  We don't
evaluate the body until it's needed, and by then, the functions are both in the
environment.  However, these solutions would require re-parsing any blocks to be
evaluate after the operator definition.  One optimization here is to mark all
blocks ever seen as dirty upon reaching an operator definition and then marking
them clean once they've been re-parsed.

Possible solutions:
* parse <-> eval
* preprocessor: find ops/macros, add to lexer+parser, re-read

* scoped operators
* only allow operator definitions at top level
* importable operators

Shuld we require that macros be defined in a new file?  This splits the
preprocessor directives cleanly apart from the "real" program.

The interpreter should be Read, Preprocess, Lex, Parse, Evaluate, drop a new environment, get a new file, loop.

Some operators that might be worth showing:
* .. and ... (exclusive/inclusive range)
* binary logic


Macros/Objects
--------------

If someone wants to make a `List` with `append`, `pop`, etc.

Operator preprocessor strategy:

        lexer          parser       preprocessor                                   evaluator
source ------> tokens -------> AST -------------> (lexer, parser, evaluator, AST) ----------> output

preprocessor:
  1. find operator and macro statements
  2. add operator token to lexer, add op rule to parser, add eval rule to evaluator
  3. figure out what to do about macros

Design decisions:
* Make them say unop vs. binop: no magic.  One way to do this would be to define
  an operator and inspect the function's arity to determine whether to call it a
  unary or binary operator.  I'd rather force novices to be explicit with their
  choice in defining an operator.  This also provides a place to create a
  meaningful error message that the function is the wrong arity for the type of
  operator specified.
* Binop tokens are always left associative: no particular reason.  I probably
  did this because most binary operators novices would have encountered at this
  point are left-associative arithmetic operators.


Too Many Layers
---------------

I had originally tried to define these parsers explicitly as state machines in
Python.  It didn't work.

Instead, I wrote my own parser generator sort of modeled after the other
generators I had used.


Unquote with program counters
-----------------------------

unquote: expr -> AST

AST can be expr or block.  If it's a block, we need to assign PCs.  Actually, we
need to assign PCs in any case, because some expressions can be further made up
of blocks (`if`, for example).

Macro expand: `unquote(B)` takes the result of `eval(B)` and inserts it into the
AST location of `@(B)`.  If `eval(sym)` is a block, replace unquote with a jump
into `PC + (0,)` (unless it's already taken), and set PCs for the new block.
Otherwise, replace unquote with the expr.  THIS DOESN'T WORK.

Part of the problem is that we can't have a single macro expansion phase in the
evaluator.  Originally, the first part of `eval` was `expand`, and any unquote
found thereafter was an error.  This doesn't work because some unquote
expressions are more than just simple name lookups.  For example, in the
`switch` macro, we find `@(bodies[@i])`.  These complicated expressions require
evaluating their argument to be able to get the correct AST to insert.

Should `eval` handle unquote?  In `x = @B`, `x` is now an AST.  What can we do
with it?  Walk, eval, compare, modify.  This seems like a bad idea.

Instead, `expand_macro` recursively at runtime.  Parse time isn't going to work
because we can't eval `@($macrosym_1[$macrosym_2])` until these variables are
set.

Does this accomplish caching macros?  we can figure that out later.  That
doesn't seem like we want it anyway, so we can just bloat the PC store for now.

It's okay for `$macrosym_` variables to leak because they can't be spelled
anyway.  Also, that's the whole reason we gensym'd them in the first place!


I hate goto
-----------

So let's get rid of it!

We now have choices in our looping construct:
1. `WHILE block DO block END`
2. `DO block WHILE block DONE`
3. `LOOP block UNTIL block END`
4. `REPEAT block WHILE block END`

(4) is actually a duplicate of (2), and all the rest can be made out of macros
of each other, so I'll stick with the standard while loop in (1).

What does this need to support?
* `for i \in [0, 10) do ... done`
* `while True do ... done`
* `do ... while False done`

To me, a loop is (setup, condition, body, update) as blocks.  `for` can be made
from `while`, as can `do-while` or whatever else you want.  In http://forth.com
words, you can make a definite loop out of an indefinite loop.

And in going through this, I've realized that you can actually go one further
and follow the 7DS paper's recommendation and make every loop a `while True` and
add a `break` statement.


git log
=======

It turns out that PLY *can* do most of what I wanted.  Macros almost worked in
it, since I was able to programmatically create a PLY lexer+parser based on the
default settings and collections of tokens+rules I wanted to add.  The biggest
problem with it is that I couldn't get a partial parse.  I probably could have
just created a `next_statement` rule that was just `^(STATEMENT).*$` and asked
for its string length and continued parsing from there.

### 50b22ef524a54b402304167dfcc24a773dd797c7

Attempt switching to statement-by-statement parse-eval strategy (ref #27)

It doesn't look like this will work using PLY.  It doesn't implement the
YYACCEPT macro from yacc, so it won't stop parsing until it's exhausted all
input tokens.  There's no way to just grab the first statement and leave the
rest unparsed.

Things to look into:
* pyPEG https://fdik.org/pyPEG/
* Arpeggio http://igordejanovic.net/Arpeggio/
* Grako https://bitbucket.org/apalala/grako
* pybison https://github.com/smvv/pybison

pybison was written in Python2 and I didn't want to go through and convert it to
Python3 before I could use it.

pyPEG seems very close to what I built for my parser generator, actually.  I've
been trying to figure out why I didn't use it, and I'm almost certain it's
because I couldn't figure out how to get an AST out of it.  Either that, or I
never actually saw `csl()` and thought that I had to make `optional(sequence(X,
comma))` or something like that.  Looking at it now, I really think it's that I
couldn't handle the way it returned values to me, because I was going to be
extracting data out of a bunch of anonymous rules once macros showed up.

### 15ce2513a02404ddd17c3e11628a06d19c54471f (interpreter-style)

This is going to be impossible.

I tried to augment the data passed through the lexer+parser with text position
data, but it required way too much manual intervention.  I don't mean to say
that this was impossible, because it definitely could have happened.  I just
thought it was too much work at the time.

### 89f52ae3d695e125a127347bc5d1b5df85d9502d (arpeggio)

Keywords and operators are going to be really difficult!

The issue here was that my identifier rule had to somehow be prevented from
matching any keywords.  This grew into a very complicated matching rule.  The
`(Not(...), Regex('.+'))` didn't seem to match well.  (I used `.*`, why?)

### 3b123e8c93794a92fe1e1cd31550474b07712013

End of experiment.  Redesign required.

I got stuck because I don't know anymore if len() is being called on a Match
object or a list.  I think I need to be able to separately answer the following
questions in each combinator:
* Does a prefix of this string match?
* Where in the source did this string match?
* What should show up in the AST for this match?
* Where in the source should I start looking for my next match?

### 5ee002c8d34789b1ed6b71b9af505baf5e3dc9fd

Allow literals and reserved words to update identifier rule

A tricky thing about the parser is that it requires that the rules have access
to `expr_types` and other mutable data structures.  Here, `literal` was defined
as a `choice` parser, but that couldn't update if `literals` ever changed
because it was fixed at that time.  Instead, we make a parser each time based on
the current state of the `literals` list.


There were some very strange design and implementation decisions that came about
because of `goto`.  Because `goto` was restricted to only jumping to labels
within the current scope, I had to be able to determine all the labels in the
current scope before evaluating a function (in case a `goto` showed up).  This
didn't work at the top level because of macros and the inability to parse more
than one statement ahead, so we changed the rule to be that you could only jump
to a label statement that had already been "seen".  This was also terrible, and
eventually started the move away from `goto`.

It was very difficult to figure out a way to make `goto` work with the
Steele-style layered interpreter.  There was no way to easily stop executing
statements and restart execution at the correct location without some sense of
where in the program each label was.  The original attempt at associating the
label with a block was an attempt to get around this, but I was throwing a
special exception on encountering a `goto` and it was impossible to know which
level of nested block evaluations should abort.


### 6fd2aec1568f14f4e4e1e9fa060754abc8dc2c04

Implement hack for macro expr expansion

Since goto may be necessary in a macro_expr, we need to be able to evaluate
those gotos without ruining the appearance that this macro is just a single
expression.  This can be accomplished by wrapping the macro body in an
immediately-invoked function expression.  However, to preserve the semantics of
macro evaluation happening in the current scope instead of a child scope, there
needs to be a function that doesn't create a new environment.

### ddc848e946dd63835337ab9a78de9b2740ffdfa1

Lift floats in evaluator

Who knew that you couldn't describe roots rationally?

### 9bf08208c69dcf8a2ff35d20b6b3c3f8aa34f406

Program counters and macros don't mix well

It's difficult to assign program counters at the right time during the expansion
of a macro.  Immediately post-parse doesn't get any pcs for the statements it
catches (even though it seems like it should). Unquote is interesting to think
about as "double-eval", and that idea might survive this branch.

These program counters were interesting.  Every statement in the program was
given a program counter that represented how nested into the program it was, and
which statement it was in that level of nesting.  The program counters were
Python tuples.  The first statement at the top level in the program had PC=(0),
the second had PC=(1).  Any sub-statements of the second statement (with PC=(1))
would have PCs of (1, 0), (1, 1), and so on.  Each layer of nesting would add a
new field to the tuple.  Luckily for me, Python sorted these exactly as I wanted
them to: (0) < (1) < (1, 0) < (1, 1) < (2).

This ended up not working though, because macros needed to insert new statements
into the program so they could be executed.  Inserting them wasn't a problem;
guaranteeing the consistency of program context like environments and function
return locations was.  This probably could have worked by adding more state to
the interpreter and assigning even more program counters (maybe one to each AST
in the entire program, regardless of AST type).

### 28131b0f9b468795bd8778d7bed108a33f13fac8

Turn everything but macros into an expression

`if` and assignment both became expressions here that returned the values you
would expect.


Bitbucket issues
================

### #19: String replacement macros

`replace CLOSE_EIGHT $'))))))))'`

This is actually really hard. It's going to have to be a real preprocessor
directive, because it breaks the parser, so preprocessing needs to be run before
anything to do with parsing. Which means there needs to be a new parser specific
to preprocessing.

These preprocessor directives might be forced to be in a different file or they
could be required to be the first things in the file (that way the parser can be
"static" when the real program starts).

### #21: Type declarations

It would be interesting to have a type declaration syntax which would throw
errors at runtime (because there is no such thing as compile-time) if a variable
was assigned to an incorrectly-typed value.  This could help transition to
languages that are statically typed.

This also brings up the question of what kind of type system this language has.
The variables are all dynamically typed, and there are no interoperations
between Boolean, Number, Function, and Dictionary, so I'm not entirely sure what
that falls under.  Strong typing?

### #22: Classes

How would you use macros and functions/dictionaries to be able to pretend like a
real object system exists?

### #28: Macro scoping

`function () {macro_block} ... end` should scope the macro to the function
instead of being globally in the parser

There should be some way of removing rules from the parser when they're no
longer relevant.  One way to do this is to have scoped rules, like shown above.

### #15: Find a name

Palette, Gesso, Escalera


Other notes
===========

This is a very unopinionated language.  I wanted to give the user as much
freedom as they would like to use.

Macros require names so they can be referenced in the evaluator.  There's
nothing wrong with an anonymous macro that gets a name via gensym, although one
feature of naming them is the ability to re-define the macro syntax or
transformation pattern mid-program.

It's kind of weird that the evaluator has all the setup required for
assignment, but there are no grammar rules for it by default.  One
justification is that we needed environments because of functions, and
that just got exposed in the AST possibilities because it's so common.

http://www.oilshell.org/blog/2016/11/01.html

Racket's `#lang` is cumbersome for a novice [citation needed]
(https://docs.racket-lang.org/guide/hash-lang_reader.html)

I forgot about comments!

The language was intended to break programming into two parts:
   1. Determine the problem domain and create some abstractions relevant to that
      domain
   2. Solve the problem using your abstractions

This is best implemented when there is actually an interesting and nontrivial
problem to solve.

I might want to bring operators back as separate from macros.  It would provide
a place to teach a lesson about associativity and operator precedence.

It would be interesting to see if I could turn my macro syntax rules into
railroad diagrams.

"The Call of the While"

"cardinality" is a better word than "repeat_count"

http://www.engr.mun.ca/~theo/Misc/exp_parsing.htm

One assignment could be to remove dictionaries and make students build them out
of functions.

https://harc.ycr.org/flex/
https://news.ycombinator.com/item?id=13966054


Notes from writing
==================

IEEE754 could be implemented by a student with reasonable math background.  It
would be slow, but it's definitely possible to have "real" floats in this
language.
