\chapter{Base Language}
\label{chap:language}

In this chapter, we will describe the features of base language in our
interpreter framework.  The features were chosen to minimize complexity when
presented to a novice programmer without excessively limiting the capabilities
of the language.  Many of the choices were made following the introductory
language design principles set out by McIver and Conway in \cite{7ds}.  We
wanted a minimal set of orthogonal features that worked the way a non-programmer
would expect.  We also wanted some features that would allow novices to move to
other languages with few changes to their models of how these features worked.

In the first section, we will list the primitive features we chose to include in
the base language.  Next, we discuss the features we explicitly omitted.
Finally, we describe the extensions to the language in the form of runtime
macros and startup settings.

\section{Primitives}

The primitives in the base language include basic value types, the unordered
collection type, functions, and control flow mechanisms.  In this section, we
describe our implementation and rationale for each type.

\subsection{Data Types}

The data types included in our base language are numbers, strings, booleans, the
null value, functions, and unordered collections.

\paragraph{Numbers}
The base language implements numbers as arbitrary-precision rationals.  We
wanted to avoid teaching beginners about numeric representations (for example,
two's-complement arithmetic or IEE754 floating-point numbers) because students'
prior knowledge of basic arithmetic should transfer into programming.  With
rationals as the only numeric type, some arithmetic operations, such as square
roots and logarithms, must either be approximated or represented symbolically.
We considered providing access to a symbolic algebra system like SymPy
\cite{sympy}, but we chose against it in order to avoid complicating the
notional machine with ``exact'' and ``inexact'' versions of different
calculations.  Instead, we approximate any calculation involving irrationals as
a rational number.

\paragraph{Strings}
The base language implements strings as immutable sequences of characters.  We
wanted to avoid problems with null-termination, mutability, and character
encoding in our string implementation, so we implemented strings as their own
data type.

\paragraph{Booleans}
The base language implements booleans as the singleton values \texttt{True} and
\texttt{False}.  We wanted to avoid the question of truthiness, so we
implemented booleans as their own literal type instead of mapping them onto 0
and 1 or introducing the notion of truthy and falsy values.

\paragraph{Null}
The base language implements the null type as the singleton value
\texttt{Nothing}.  This is equivalent to Python's \texttt{None}, Ruby's
\texttt{nil}, or JavaScript's \texttt{null}.  There is no equivalent to
JavaScript's \texttt{undefined} type.

\paragraph{Functions}
The base language provides a general mechanism for creating anonymous functions
but not for creating named functions.  Many languages conflate the concepts of
defining a function and giving it a name, so we implemented only anonymous
functions and allow them to be assigned to variables in the environment in the
same way that all other values are.  Functions must take a constant number of
arguments, so they cannot have optional arguments or variadic arguments.
Instead of using an explicit \texttt{return} keyword, functions return the value
of the last statement that was executed in the body of the function.

\paragraph{Unordered Collections}
The base language implements collections as immutable key-value stores.  We
wanted the keys and values to be of any type, including unordered collections
themselves.  Using a mutable object as a key in a collection introduces the
potential for key collisions if that object changes.  We avoided the issues of
unexpected key changes by making these collections (and all other primitives
data types) immutable.


\subsection{Control Flow}

The base language includes two forms of control flow: conditional execution and
indefinite loops.  Both of these forms are expressions rather than statements so
that they can be used as the final expression of a function body.

\paragraph{Conditional Execution}
We chose to implement conditional execution in its ternary form: a condition, a
consequent, and an alternative.  The consequent is executed in the case that the
condition is satisfied; otherwise, the alternative is executed.  A conditional
expression evaluates to the value of the executed branch.  We considered this
form of conditional to be simpler than other forms, such as a pattern-matching
construct (such as \texttt{match} in Scala) or a construct with multiple
conditions (such as C-style \texttt{switch} or Lisp's \texttt{cond}).

\paragraph{Indefinite Loops}
The looping construct we chose to implement was a loop that repeatedly executes
its body undil a condition is no longer satisfied usually referred to as a
``while loop''.  We felt that this form most closely matched the indefinite loop
constructs in other languages.  In our loop structure, the condition is executed
first, then the the body is executed if the condition is satisfied, and this
process continues in a loop.  The indefinite loop is an expression that
evaluates to the value of the last execution of the body.


\section{Omissions}

We intentionally omitted some common programming language features from the base
language.  Notably missing from the list of features in the previous section are
variable assignment and declaration, ordered collections, definite loops, and an
object system.  We chose not to implement these features because their syntax
and semantics vary across languages.  As part of our extensibility goals, we
wanted to give instructors the freedom to include these features (and any
others) at their discretion.

\paragraph{Assignment and Declaration}
The first problem with implementing assignment and declaration was the choice of
syntax.  For conformity with most programming languages, we would provide syntax
that assigns the name \texttt{x} with the value \texttt{10} using
\texttt{x~=~10}.  However, using the equals symbol for assignment is confusing
to novices \cite{bayman}, so some languages use \texttt{x~<-~10},
\texttt{x~:=~10}, or some other syntax.  We encountered our second problem when
deciding whether to include declaration as a separate syntax.  In Python, for
example, there is no separate declaration syntax, and a variable binding is
created during assignment if a binding does not already exist.  If a separate
declaration syntax does exist, there is also the question of whether to make a
distinction between constant and variable bindings.  In JavaScript, as another
example, declaring a variable with \texttt{let} or \texttt{const} makes the
binding variable or constant, respectively.  Rather than include any default
syntax or semantics, we chose to implement the interpreter with enough
flexibility to let an instructor decide what to do.

\paragraph{Ordered Collections}
Most languages use the same syntax for accessing the elements of ordered and
unordered collections.  This requires that novices associate the same syntax
with multiple ideas (index of a list and key of a dictionary, for example),
which can be confusing to a novice.  Extending the language to include something
that acts like an ordered collection is possible using macros, which gives the
user the freedom to make choices such as whether index numbers should start at 0
or 1.

\paragraph{Definite Loops}
Almost all languages with looping constructs include both the definite and
indefinite loop, although the forms of their definite loop differ.  Some
languages have a definite loop in which an iterator variable takes on values in
some iterable (Python, Ruby), while others break the loop into a setup,
condition, and update (C, Java, JavaScript).  We saw that the indefinite loop
could be used to implement any of these definite loops, so we designed the
interpreter to allow a user to implement this through the extensibility
mechanisms.

\paragraph{Objects}
Object-oriented programming can be a confusing way to introduce novices to
programming concepts.  Students learning object-oriented programming are forced
to contend with method resolution and instance state in addition to
understanding functions, variables, loops, and other constructs.  Objects are
useful abstractions for experienced programmers, but they complicate the
notional machine, and the syntax and semantics of object systems differ across
languages.  For these reasons, we chose not to implement an object system in the
base language.  However, the extensibility features of the interpreter make it
possible for a user to implement an object system of their own design.


\section{Extensions}

The power of this language and interpreter framework comes from the
extensibility features.  The user can modify the grammar of the language using
runtime macros.  In addition, the instructor can modify the interpreter through
start-time settings files and through modifications to the interpreter itself.

\subsection{Macros}

Our goal for macros in this language was to allow users to extend the syntax of
the language at runtime in order to add syntactic primitives they find useful or
avoid repetition of common program structures in ways that functions were unable
to.  We wanted to make this process accessible to an introductory-level
programmer, so we wanted to avoid requiring users to traverse ASTs or modify
program text directly when implementing macros.  We considered three existing
macro systems: C preprocessor macros, Lisp runtime macros, and Rust
compile-time macros.

The C preprocessor provides two types of macros: object-like and function-like.
All macros are simultaneously expanded at compile time, and each macro is
defined with a name (all uppercase by convention) that indicates to the compiler
that macro expansion should occur.  Object-like macros (such as
\texttt{\#define~PI~3.14}) take no parameters and are generally used for
constants.  Function-like macros can take parameters (such as
\texttt{\#define~ADD(x,~y)~x~+~y}), that are expanded in a call-by-name fashion.
The text substitution causes problems with operator precedence
(\texttt{a~*~ADD(b,~c)} becomes \texttt{a~*~b~+~c}), which is usually solved by
adding more parentheses in the expanded text.  There are other problems with
expanding complex statements in this way, although users of these macros have
developed conventions for avoiding these
errors\footnote{\url{https://stackoverflow.com/questions/1067226/}}.

Lisp macros have much more flexibility than C macros, and the homogeneous syntax
of Lisp allows them to avoid the issue with operator precedence, as well as
other issues faced by the C preprocessor.  Lisp macros are expanded at runtime,
and they are defined as an s-expression prefix and a transformation function
from the source expression to a target expression, usually manipulating the
source list directly.  Because Lisp syntax is entirely made of lists, the
function creates a list containing the symbols and values in the target
expression.  Different Lisp dialects provide different mechanisms for defining
macros, but these are usually convenience functions around the same basic
idea\footnote{\url{https://docs.racket-lang.org/guide/pattern-macros.html}}. Common
Lisp has \texttt{defmacro} and Scheme has \texttt{define-syntax-rule}, for
example.

Rust macros are similar to C macros in that they are expanded at compile-time,
but they avoid some of their pitfalls (operator precedence and variable
shadowing, for
example)\footnote{\url{https://doc.rust-lang.org/book/macros.html}}.  Rust
macros are defined with a name, a syntax pattern, and a description of the
replacement syntax.  Metavariables captured in the syntax pattern are tagged
with what kind of syntax they capture (expression, identifier, type, etc.).  To
use repetition, the macro is defined with special operators in the pattern
(\texttt{\$(\$x:expr),*}), and the same operator is used in the replacement code
(\texttt{\$(println!(\$(\$x),*))}).  A macro defined this way would capture any
number of comma-separated expressions, and use them as arguments
(comma-separated) to the \texttt{println!} function.

In each of these languages, macro usage appears exactly like a function call
(except for object-like C macros, which appear exactly like a variable).
Without careful attention to macro name conventions, it can be difficult to know
ahead of time whether that ``function'' will be a normal function or a macro.

Our design for the macro system has the following goals:
\begin{enumerate}
\item The syntax of macros should not be the same as function calls.
\item Macro expansion should perform substitutions on the syntax tree instead of
  on the text.
\item Users should not have to worry about name collisions between macros and
  the larger program.
\item Defining a macro should require minimal knowledge of syntax trees.
\item A user should be able to define a macro anywhere in the program.
\item The macro expansions should happen at runtime.
\end{enumerate}

We used the pattern-matching idea from Rust in the form of a mini-language for
defining captured syntax patterns, but we omitted the metavariable syntax-type
annotations in favor of always capturing an entire expression or block.  We
allow the user to decide when to evaluate any captured code with a Lisp-style
unquote operator.  This mini-language is described in detail in
\Cref{sec:macro-mini-language}.

\subsection{Startup Settings}

Some modifications to the language would be difficult or impractical to
implement through macros.  For example, the standard mathematical operators
would be difficult to implement as runtime macros due to the necessity of
operator precedence.  We could have added more special syntax for defining unary
and binary operators, but we found it easier to specify these language-level
changes at interpreter startup.  Part of our goal was to make these
modifications easy to define and easy to share.  For example, an instructor may
want to provide some operators for one assignment and a completely different set
for another (boolean operators versus set operators, for example).

Our interpreter design includes the ability to provide a settings file
describing these language modifications at startup.  This settings file contains
the definition of any included operators and their precedence levels, some
simple feature flags, and a mechanism for arbitrary changes to the grammar.  The
relevant fields in the settings file are described in \Cref{chap:grammar} and
\Cref{chap:evaluator}.

\section{Summary}

Our goal for the base language was to implement a minimal set of teachable
features, including common data structures and a function abstraction.  Some
examples of programs are included in \Cref{app:programs}.
