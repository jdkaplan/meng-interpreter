\chapter{Introduction}

There is no shortage of programming languages directed at novice programmers,
and these include Logo \?, PLT Scheme (now known as Racket) \?, and Scratch \?,
among others.  This project aims to add yet another language to that set.

The driving philosophy behind this language is that the power of programming is
the ability to create procedural and informational abstractions that allow
complex problems to be solved.  One of the frustrations in teaching introductory
programming with modern languages is that many interesting and illuminating
programming exercises are trivially solved for students in the standard
libraries \?.  Also, modern languages are complicated and feature-filled, which
causes instructors to choose between artificially limiting themselves to a
subset of the language they teach or teaching too many concepts at once \?.

This language is not intended for use in a production environment.  For novice
programmers, performance of the interpter is not important \?, especially if it
comes at the expense of simplicity or clarity.  One of the goals for this
language is to allow instructors to present a (relatively) simple notional
machine for reasoning about programs before they are executed.

Many of the quirks of this language come from the inclusion of syntactic macros.
The language syntax was chosen to be similar to those used in courses that
require programming as a prerequisite \?.  We included syntactic macros as a way
of emulating the richer syntax and features of modern languages like Python,
Java, and C++ by allowing students or instructors to implement exactly those
which they deemed useful or interesting.


\section{Interpreter Structure}

This interpreter can logically be broken down into four parts: the parser
generator, the grammar, the evaluator, and the REPL.  The parser generator
implements the text-matching primitives and combinators we needed in order to
specify the grammar rules for both the base language syntax and macro rule
syntax.  The grammar specifies the base language syntax, the data structures for
extensible rule types, and the transformations from program text to ASTs.  The
evaluator evaluates the program as specified by the AST created by the grammar,
producing the return values of statements and changes to program state.
Notably, the evaluator has direct access to the grammar, which allows it to edit
the extensible rules.  The REPL exists to read program files (or interactive
input) and display any relevant values to the user.

The interpretation path starts with the REPL constructing a parser from the
grammar, constructing an evaluator from the parser, and then reading in the
program text.  The parser then finds the first complete statement in the program
and emits its AST and the string length of the match.  The evaluator then
evaluates the AST, returning the resulting value and performing any necessary
modifications to the parser.  The REPL then displays that result and resumes
parsing the program from the end of the previous match.  This process continues
until an error occurs or the program text is exhausted.


\section{Language Primitives}

The following is a list of features in the base language:

\begin{itemize}
\item Arbitrary-precision rational numbers
\item Booleans
\item The null value
\item Functions of a specified number of arguments
\item Key-value dictionaries (almost identical to Python's)
\item If-expressions
\item While-expressions
\item Runtime macros
\end{itemize}

This language far from the minimal set of primitives required to be a
Turing-complete language, but this is a suitable set of primitives that any
programmer can expect to have in a modern production language \?.  These
primitives are also those suggested by \cite{7ds}, with the exception of the
looping construct.
