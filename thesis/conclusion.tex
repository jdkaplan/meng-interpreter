\chapter{Discussion}
\label{chap:conclusion}

We conclude this thesis with a discussion of the alternate choices for some of
our design decisions and a list of possible future work.

\section{Alternate Design Choices}

Our original design for the language included three features not present in the
final version:
\begin{enumerate}
\item the \texttt{goto} statement, which we eventualy found too difficult to
  implement properly in conjunction with other language features
\item feature blocks that could be combined in a way to produce a full
  interpreter with those features
\item a mini-language for defining operators within the language itself
\end{enumerate}

\subsection{Goto}

Part of our goal in creating this language was to have novice programmers
understand common programming constructs by implementing them within the
language itself.  As part of this goal, our original design did not include any
looping constructs at all.  We intended to have users implement their own
\texttt{while} and \texttt{for} loops (or \texttt{do-while} and
\texttt{for-each}) in order to see exactly how control flow operates in each of
these loops.  This required having a flow control construct more basic than a
loop, and we decided that we would accomplish this by providing labels and the
\texttt{goto} statement.

We were wary of the problems that \texttt{goto} can bring \cite{goto-harmful}.
We wanted to avoid the possibility of unintentionally jumping out of one
function into another, so \texttt{goto} was only allowed to jump to a label that
was defined within the same scope.  To achieve this, we had to be able to
determine all the labels in the current scope before evaluating a function (in
case a \texttt{goto} appeared later in the function).  However, this approach
did not work outside of functions because of the inability to parse more than
one statement at a time (in the case that a statement was a macro), so we added
the restriction that \texttt{goto} could only jump to a label that had already
been ``seen'' in the course of evaluation.

This decision made it difficult to implement the evaluator.  The state of
evaluation had to include what statement was currently being executed and where
in the program it was.  Environments had to store the mappings from labels to
program locations, and each statement was assigned a program location once it
was parsed.

Our intended solution to handle macros was to expand the macro body and insert
the resulting block at the location of the macro expansion.  This led to the
idea that program locations could not simply be integers (unless we wanted to
re-assign every program location on every macro expansion), so we implemented
program locations as Python tuples.  Each statement's program location
represented two things: how deeply nested into the program its containing block
was, and the position at which it fell within that block.  The first statement
at the top level in the program had location (0), and the second had location
(1).  If the second statement was a function definition, for example, any
statements within that function's body block would have locations of (1, 0), (1,
1), and so on.  Each layer of nesting would extend the length of the tuple by
one element, assigning the last element to zero for the first statement at that
level.  The default Python sort order on tuples happens to order these program
locations correctly (\texttt{(0) < (1) < (1, 0) < (1, 1) < (2)}).

\pagebreak

Conditional execution and \texttt{goto} were implemented as special
\texttt{jump} statements that were inserted into the program as needed.  These
would cause program evaluation to immediately restart at a specific program
location.  We realized that a real call stack would be necessary to ensure the
ability to return out of a function correctly and that the implementation of the
evaluator would be cleaner as a virtual machine instead.  Rather than complicate
the language implementation further, we decided to remove \texttt{goto} from the
language and implement \texttt{while} in its place.

\subsection{Feature Blocks}

As part of the extensibility goals of the language, we wanted an instructor (or
other language maintainer) to be able to add features to the language that were
more complex than our macro system would allow.  Our original plan was to
structure the interpreter in a way that was inspired by the strategy outlined in
\cite{composing-monads}.  The language would be broken up into feature sets, and
each feature set would require a parser and evaluator.  Each feature set would
be implemented by a feature block, and these blocks could then be composed in
different ways to form interpreters for different languages.

At the time, we were still experimenting with existing parser generators, so we
only implemented the evaluator in this style.  Due to the recursive nature of
stacked evaluators and Python's lack of tail-call optimization, any non-trivial
program caused us to exceed Python's recursion limit.  One choice we had was to
implement this in a language that provided tail call optimization, but we
decided to re-structure the evaluator in a more iterative style in order to
support \texttt{goto} (which was still in the language at that point).

The ability to toggle features eventually returned in the form of the settings
file, but adding entirely new features to the interpreter still requires an
understanding of the other features already present.

\subsection{Operator Mini-Language}

Our original extensibility features included a mini-language (similar to the
macro mini-language) for defining new operators from within the language itself.
The original syntax used a keyword \texttt{operator} and required specifying
both the string to use as the operator symbol and the function to use as its
evaluator.  We used the arity of the function to determine whether to create a
unary or binary operator (or error, in the case of a higher arity function).
This automatic operator arity detection goes against our learnability goals, so
we split the \texttt{operator} keyword into \texttt{unary\_operator} and
\texttt{binary\_operator}.

In this formulation, each new operator defined was at a lower precedence than
any previously defined operators.  This allowed a user to define operators
without knowing the existing precedence hierarchy, at the expense of forcing a
specific ordering of operator definitions.  However, this made it impossible to
add a new operator to a pre-existing precedence level or define two new
operators at the same precedence level.  We considered an extension to the
mini-language in which the precedence level was included in the operator
definition, but chose not to implement it because this required the user to
design an entire precedence hierarchy before defining a collection of operators.

Operators appear to be a special-case of macros, so it seems possible to use the
macro system to regain this function without its own mini-language.  However,
this is only true for prefix operators.  In the case of infix and suffix
operators, the macro begins by looking for an \texttt{expr}, which causes
infinite left recursion in the parser.  Although there are ways of automatically
re-writing rules to avoid left recursion \cite{left-recursion}, we cannot
guarantee this will work for the non-regular rules in our grammar.  Ultimately,
we decided that instructors would be better equipped to define custom operators
for the language, so we moved all operator definition into the settings file.


\section{Future Work}

We note two important improvements to be made on this work.  First, there are
still many opportunities to improve the interpreter framework, both in terms of
usability and efficiency.  Second, the language design can be improved with
feedback from real programmers at any experience level.

\paragraph{Modules}
There is currently no notion of modularity among files being executed by the
interpreter.  All files are run in the same global environment with no
separation between them, which can cause unexpected problems if variable names
are reused across files.  This can be remedied by implementing a module system
with a new \texttt{import} statement.

\paragraph{Temporary Macros}
One problem with macros is that there is no notion of a temporary macro.  Once a
macro has been defined, it exists in the grammar until the program finishes
executing.  One way to accomplish this is by adding a new type of statement that
allows for the enabling and disabling of an existing macro.  Another way would
be to provide a mechanism for restricting the use of a macro to a certain
scope.

\paragraph{Feature Blocks}
Now that \texttt{goto} has been removed from the language and the parser can be
modularized, we can return to the original interpreter structure with feature
blocks.  Each block can define grammar rules and data structures along
with registering evaluator functions for specific AST node types.  This allows
for easy addition, removal, or modifcation of entire language features.

%% \paragraph{Objects}
%% One simple object system we considered implementing was equivalent to C or Go
%% structs, in which are the objects are collections of named fields and the
%% methods are functions that take an object as their first argument.  This avoids
%% the inheritance problem, but it also makes object introspection difficult.  It
%% becomes impossible to tell what methods exist on an object.  The second object
%% system we considered used unordered collections as objects with the methods and
%% fields being elements in the collection.  We decided against implementing this
%% as its own syntax because it behaves exactly like unordered collections already
%% do, and we would simply be adding syntactic sugar.

\paragraph{Evaluation}
Our primary goal was to make a language that was easy for students to learn, but
we have not shown that how this language compares to other languages in that
respect.  Future work can include experiments or surveys to determine how
properties of this language affect the rate and quality of learning.
