\chapter{Grammar}
\label{chap:grammar}

In this chapter, we describe the implementation of a grammar for the base
language using our parser generator framework.  Our main goal for the language
was to allow it to be introduced to the novice incrementally, with each new
piece of syntax form being associated with a new programming concept.  Our
secondary goal was to provide the novice with tools and knowledge that would
transfer to other programming languages, when possible.  Toward this end, we
developed the grammar with the following design philosophy:

\begin{enumerate}
\item Provide exactly one syntax for each data structure or control flow form
\item Avoid using the same symbols for different purposes
\item Use keywords instead of symbols to denote the start and end of blocks
\item Use conventions from existing languages where possible
\end{enumerate}

In our framework, a grammar is represented by a \texttt{Grammar} object, a
namespace containing syntax rules and the additional data structures they depend
on.  These rules and data structures are exposed to the rest of the interpreter
so the interpreter can make changes to them in response to macros or interpreter
settings.  The rules are used to parse program text and create the AST we use to
execute the program.

In \Cref{sec:grammar-rules}, we describe the base language syntax rules
themselves.  In \Cref{sec:grammar-mods} we describe strategies for modifying and
extending the the base grammar to accommodate macros and custom operators.

\section{Rules}
\label{sec:grammar-rules}

In this section, we discuss the rules defined in the base grammar.  The majority
of these rules are defined using the primitives and combinators provided by our
custom parser generator (\Cref{chap:generator}), although some rules are
implemented by Python generators directly.

Rule definition order is important to a proper implementation of a grammar.
Because a \texttt{Grammar} is created by executing a normal Python program, it
is possible to encounter a \texttt{NameError} by using a Python variable before
it has been defined.  This means that co-recursive rules are defined in a way
that avoids referencing nonexistent variables (similar to the
\texttt{palindrome} rule in \Cref{fig:palindrome}).  In this section, we
describe any rules defined in this way as if they were normal rules, for
clarity.  We also omit discussion of most callback functions, as they are only
used to construct the AST for later use in the evaluator
(\Cref{chap:evaluator}).

We begin with a discussion of some special rules and then continue into the four
main sections of the grammar: data structure rules, control flow rules, unary
and binary operator rules, and the macro mini-language.

\subsection{Special Rules}
Our grammar includes some special rules that are often used to define other
rules, even if they are not specific language constructs themselves.  These
include whitespace, expressions, statements, and blocks.

One of our goals was to let programmers decide their own indentation and
whitespace conventions, so we minimized our use of required whitespace.  It is
possible to add significant whitespace to the grammar by storing the current
indentation level as part of the parser state.  This would further complicate
the grammar, and goes against our principle of using keywords to delimit blocks,
so we chose not to implement it in the base language.

However, we did enforce the requirement that there could be at most one
statement per line.  Multi-statement lines can be very useful, but these cases
are rare at the introductory level.  Changing this grammar to use a statement
separator (such as a semicolon) is as simple as changing the separator for the
\texttt{block} rule defined later in this section.

\paragraph{Whitespace}
For our grammar, we recognized four different kinds of whitespace: required
multi-line whitespace (\texttt{ws\_all}), optional multi-line whitespace
(\texttt{ws\_all\_star}), newlines (\texttt{newline}), and optional single-line
whitespace (\texttt{ws\_one\_line}).

\begin{figure}[htbp]
\caption{Whitespace rules}
\label{fig:whitespace rules}
\begin{verbatim}
ws_all = Regex(r'\s+')
ws_all_star = Regex(r'\s*')
newline = Regex(r'\n+')

def ws_one_line(source, position):
    for match in parse(ws_all_star, source, position):
        ws, *_ = re.split(r'\n', match.text)
        yield Match(ws, position, ws)
\end{verbatim}
\end{figure}

The definition of \texttt{ws\_one\_line} (optional one-line whitespace) is an
example of a rule defined manually as a generator.  The \texttt{ws\_one\_line}
rule is used in cases where we need to skip over whitespace without crossing
onto the next line (for example, in the \texttt{block} rule discussed below).

\paragraph{Expressions and Statements}
An expression is a combination of values, operators, and functions that
evaluates to a value.  Since statements also evaluate to values, the meaningful
difference between an expression and a statement is that an expression can be
used as the value in an assignment but a statement cannot.  A macro definition
is the only syntax form in the base language that is a statement but not an
expression.

The available types of expressions and statements are extensible (see
\Cref{sec:grammar-mods}), so the grammar rules for \texttt{expr} and
\texttt{statement} must be able to respond to these extensions.  We accomplished
this by defining two lists within the grammar to contain the different
expression and statement rules.  These lists are referred to as
\texttt{expr\_types} and \texttt{statement\_types} which contain references to
expression rules and statement rules, respectively.  An \texttt{expr} or
\texttt{statement} is simply a \texttt{Choice} with the associated list as the
options.  To add a new expression or statement type, we add the rule for that
type to the appropriate list, so that any later parse can make use of that rule.


\paragraph{Blocks}
A block is comprised of any number of statements separated by newlines.  In the
\texttt{block} rule, we skip one-line whitespace between statements and newlines
in order to make indentation (or trailing whitespace) insignificant.
\texttt{newline} is used as a statement separator to require that at least one
newline occurs between any two statements.  This can be changed to any other
rule (\texttt{Raw(';')}, for example), to use that as a statement separator
instead.

\begin{figure}[htbp]
\caption{The block rule}
\begin{verbatim}
block = SeparatedStar(
    statement,
    newline,
    skip_whitespace=ws_one_line,
)
\end{verbatim}
\end{figure}

A block is used for the body of any multi-statement structure, such as a
function or a conditional.  In order to provide more freedom in programs, we
chose to have conditionals and loops (discussed in
\Cref{sec:grammar-control-flow}) accept blocks for their conditions.

\subsection{Data Rules}
The rules described in this section support the primitive data types available
in the base language.  Each of these rules is an element in the
\texttt{expr\_types} list by default.

\paragraph{Numbers}
Our grammar supports numbers in either integer or decimal format, so as to
maintain compatibility with other programming languages and ubiquitous math
notation.

\begin{figure}[htbp]
\caption{Number rules}
\begin{verbatim}
integer = Regex(r'\d+')
decimal = Regex(r'\d+\.\d+')
number = Choice([decimal, integer])
\end{verbatim}
\end{figure}

Because decimal numbers become rational numbers exactly the same way that
integers do, these rules could have been implemented as a single \texttt{number}
rule with
\texttt{Regex(r'\textbackslash{}d+(\textbackslash{}.\textbackslash{}d+)?')}.  We
chose to use two separate rules to support a possible change to handling
decimals as floating point numbers.  An \texttt{integer} match is a prefix of a
\texttt{decimal} match, so the order of options in \texttt{number} is important.
The \texttt{decimal} rule comes before \texttt{integer} in the options list to
force an attempt to match \texttt{decimal} before falling back to
\texttt{integer} (in case a decimal point is not found).

\paragraph{Strings}
We had two goals for string literals: avoiding the need for escaped string
delimiters and avoiding special syntax for multi-line strings.  Our design was
based on Perl's \texttt{qq} operator, although we do not allow string
interpolation and we prefer the quoting operator to be a non-alphabetic
character.

String literals begin with a dollar sign (which is otherwise unused by many
programming languages) and a single non-whitespace character to use as a
delimiter.  We consider the string content to be every character in the file
until the next occurrence of the delimiter (unless that delimiter is preceded by
a backslash).  In addition to escaping the string delimiter, we allow the usual
special character escapes (tab characters, carriage returns, etc.) within the
string.  We accomplished this with the rule shown in \Cref{fig:string}.

\begin{figure}[htbp]
\caption{The string rule}
\label{fig:string}
\begin{verbatim}
string = Regex(
    r'\$(?P<delimiter>\S)'
    r'(?P<content>(\\((?P=delimiter)|[bfrntv\\])|[^\\])*?)'
    r'(\1)'
)
\end{verbatim}
\end{figure}

The named groups are used to determine the string delimiter and string content
in the callback function for this rule.  The callback function performs any
unescapes necessary in the content portion of the string.

\paragraph{Literals}
The base language contains three more literals: the two booleans (\texttt{True}
and \texttt{False}) and the null value (\texttt{Nothing}).

\begin{figure}[htbp]
\caption{The literal rule}
\begin{verbatim}
literal = Choice([Raw(l) for l in ['True', 'False', 'Nothing']])
\end{verbatim}
\end{figure}

\paragraph{Functions}
Function definition syntax was chosen to closely match function definition and
usage from other programming languages.  A function definition consists of a
keyword (in this case, \texttt{function}), a sequence of function parameter
names, a block of code for the body of the function, and the closing delimiter
of the function body (in this case \texttt{end}).  This syntax is described by
the rule shown in \Cref{fig:func-def}.

\begin{figure}[htbp]
\caption{The function definition rule}
\label{fig:func-def}
\begin{verbatim}
function = Sequence([
    Raw('function'),
    Raw('('),
    SeparatedStar(identifier, Raw(','), skip_whitespace=ws_all_star),
    Raw(')'),
    block,
    Raw('end'),
])
\end{verbatim}
\end{figure}

Our function call syntax is also similar to other languages: a function
succeeded by a parenthesized argument list.  The rules defining this syntax are
discussed as part of the general expression parser discussed in
\Cref{sec:grammar-ops}.  An example of both function definition and function
call syntax are described by the rule shown in \Cref{fig:function}.

\begin{figure}[htbp]
\caption{Function definition and call}
\label{fig:function}
\begin{verbatim}
show_and_square = function(x)
    print(x)
    x*x
end
show_and_square(2)
\end{verbatim}
\end{figure}

\paragraph{Dictionaries}
To maintain compatibility with other languages, we chose to implement our
unordered collection with the same syntax as Python's dictionaries, which are
similar in syntax to JavaScript objects and Go structs.  This syntax is
described by the rule shown in \Cref{fig:dictionary}.

\begin{figure}[htbp]
\caption{The dictionary rule}
\label{fig:dictionary}
\begin{verbatim}
dictionary = Sequence([
    Raw('{'),
    SeparatedStar(
        Sequence([expr, Raw(':'), expr]),
        Raw(','),
    ),
    Raw('}'),
])
\end{verbatim}
\end{figure}

\paragraph{Identifiers}
To allow maximum freedom in naming variables, any sequence of non-whitespace
characters that is not already reserved is treated as an identifier.  An
identifier cannot be the same as a keyword, for example, nor can it contain any
unary or binary operators.  This rule could not easily be written using the
combinators from our parser generator library, so we implemented it using a
normal Python generator.

Our strategy for the identifer rule was to find the longest sequence of
non-whitespace characters and find the longest prefix of that sequence that does
not contain a reserved substring.  We accomplish this by splitting the string on
each operator and keeping the longest prefix that contains no operators.
Finally, we verify that this prefix is not a keyword, a literal, or another
reseved word.

\begin{figure}[htbp]
\caption{The identifier rule}
\begin{verbatim}
def identifier(source, position):
    invalid_names = keywords.union(literals).union(reserved_words)
    invalid_substrings = reserved_substrings.union(operators)

    for match in parse(Regex(r'\S+'), source, position):
        name = text
        for chars in invalid_substrings:
            pattern = re.escape(chars)
            name, *_ = re.split(pattern, name)
        if name and name not in invalid_names:
            yield Match(name, position, Node('identifier', name))
\end{verbatim}
\end{figure}

The \texttt{identifier} rule has no callback function because it is implemented
as a parser rule directly.  The \texttt{yield} line emits a \texttt{Match} with
the data field already set as an instance of \texttt{Node} (the class we use for
ASTs).

\subsection{Control Flow Rules}
\label{sec:grammar-control-flow}
The rules in this section describe the control flow syntax in the base language.
Each of these structures is considered an expression, so these rules are
elements in the \texttt{expr\_types} list.

\paragraph{If Expressions}
Conditionals (``if expressions'') are similar to conditionals in C-like
languages (``if statements''), with the exception that the condition can be an
arbitrary block instead of just a single expression.  As usual, the ``else'' and
associated block are optional.  This syntax is implemented by the rule shown in
\Cref{fig:if}.

\begin{figure}[htbp]
\caption{The if expression rule}
\label{fig:if}
\begin{verbatim}
if_expression = Sequence([
    Raw('if'), block,
    Raw('then'), block,
    Optional(Sequence([Raw('else'), block])),
    Raw('end'),
])
\end{verbatim}
\end{figure}

An example of an if expression is included in \Cref{fig:if-usgae}.

\begin{figure}[htbp]
\caption{An if expression}
\label{fig:if-usage}
\begin{verbatim}
if
    x = 1 + 2
    x > 3
then
    print($'Big number')
else
    print($'Small number')
end
\end{verbatim}
\end{figure}

\paragraph{While Expressions}
Indefinite loops (``while expressions'') are similar to indefinite loops in
C-like languages (``while loops''), with the exception that the condition can be
an arbitrary block instead of just a single expression.  This syntax is
implemented by the rule shown in \Cref{fig:while}.

\begin{figure}[htbp]
\caption{The while expression rule}
\label{fig:while}
\begin{verbatim}
while_expression = Sequence([
    Raw('while'), block,
    Raw('do'), block,
    Raw('end'),
])
\end{verbatim}
\end{figure}

An example of a while expression is included in \Cref{fig:while-usage}.

\begin{figure}[htbp]
\caption{A while expression}
\label{fig:while-usage}
\begin{verbatim}
x = 0
while
    x = x + 1
    x < 10
do
    print(x)
end
\end{verbatim}
\end{figure}

\subsection{Expression Parsing}
\label{sec:grammar-ops}
As part of our extensibility goal, the grammar supports arbitrary collections of
unary and binary operators.  By default, no operators are included, but we
describe the process of adding operators in \Cref{sec:grammar-mods}.

Because we cannot know the available operators (or their precedence levels) at
start time, our expression parsing must be very general.  Our expression grammar
is based on the precedence climbing algorithm described in \cite{climbing}.  The
algorithm handles unary prefix operators and infix binary operators at arbitrary
precedence levels.  However, the base language includes expression syntax
(function call and dictionary access) that the algorithm does not support, so we
added special cases to the expression parser to handle them.  We could have
chosen different syntax, but those forms of function call and collection access
are ubiquitous.  Another modified version of the algorithm (demonstrated at
\cite{climbing-pratt}) supports pseudo-infix expressions and could be used
instead.


\section{Macro Mini-Language}
\label{sec:macro-mini-language}

This section describes the mini-language included in the base language that is
used for defining macros.  Our goal for the macro mini-language was to allow new
syntax to be defined at runtime in a user-friendly way.  A macro definition
consists of the following parts defined in the mini-language: the macro type,
the macro name, the macro syntax, the macro body, and any additional
metavariables required for the macro.  We first describe the syntax of macros;
we describe some examples in \Cref{sec:macro-examples}.

\paragraph{Macro Type}
The two types of macros in this language are expression macros and block macros,
which capture expressions and blocks, respectively.  Because almost everything
is an expression, the distinction between these two is small: an expression
macro is added to the \texttt{expr\_types} list, but a block macro is added to
the \texttt{statement\_types} list.  This means that syntax defined as a block
macro cannot be captured by an expression macro.

\paragraph{Macro Name}
A macro name is required because we use the name to associate the syntax and
body when making rules and expanding macros.  This also allows macros to be
re-defined later in the program.  A macro name can be any valid identifier.

\paragraph{Macro Syntax}
The macro syntax portion of a macro definition describes the syntax rule for the
macro that will be added to the grammar.  We wanted these syntax rules to be
accessible to programmers without experience writing parsers, so we chose to use
simple pattern-matching rules that we can easily transform into parser rule
functions.

In general, a macro syntax rule is a sequence of patterns, where a pattern can
be a string (which is matched exactly, as a keyword), an identifier (which
matches an expression or block, based on the type of macro), or a repetition
(which matches more complex rules).  A repetition consists of three parts: the
pattern to repeat, the cardinality of the repetition, and the separator.  The
possible cardinalities are \texttt{*} (zero or more), \texttt{+} (one or more),
or \texttt{?} (zero or one).

\begin{figure}[htbp]
\caption{The macro mini-language rules}
\begin{verbatim}
repetition = sequence([
    raw('('),
    syntax_pattern,
    choice([raw(c) for c in '*+?']),
    optional(string),
    raw(')'),
])

syntax_pattern = separated_star(choice([
    identifier,
    string,
    repetition,
]), ws_all)

macro_syntax = sequence([
    raw('syntax'),
    syntax_pattern,
])
\end{verbatim}
\end{figure}

\paragraph{Macro Body}
The macro body describes the block that will be evaluated upon macro expansion.
Rather than ask the user to create a syntactically valid string (as in C-style
macros) or tree structure (as in Lisp macros), this macro body is specified as a
single block of program text.  This allows the user to easily make function
calls, define variables, or use conditionals and loops within the body of the
macro.  The body of the macro must be able to access the blocks or expressions
captured by the syntax pattern, but we wanted to avoid the possible confusion
caused by allowing direct modification of captured ASTs.  Our solution to this
was to introduce an unquote operator (named after Lisp's \texttt{,} operator),
which can be used to access the syntax elements referred to by a metavariable.
By default, the unquote operator is \texttt{@}.

\paragraph{Macro Metavariables}
We recognize a distinction between a variable (which is a name bound to a value
in the program) and a metavariable (which is a name bound to a variable name in
a macro).  Metavariables are very much like Lisp's symbols, except that Lisp
symbols are generally valid identifiers, wheras our generated metavariables are
not.  We cannot easily determine which identifiers used in the macro body are
variables or metavariables, so we require the user to specify the set of
additional metavariables.  Any identifiers used in the syntax pattern will be
interpreted as metavariables, so it is not necessary to specify those in this
set.

\paragraph{Hygiene}
The main challenge in the macro system was providing a mechanism for automatic
macro hygiene, which makes it easy to prevent a macro from unintentionally
accessing identifiers in the main program.  A macro is expanded using dynamic
scope, so any identifiers it refers to may already exist in the scope in which
it is expanded.  This problem is not unique to this language.  Lisp solves the
hygiene problem by introducing the concept of a symbol and providing a function
that can generate a new symbol that is has not yet been used as an identifier.
We thought it would overly complicate the language to add symbols as a primitive
data type, especially without the benefits of Lisp's code-as-data
representation.  Instead, we solved the problem by automatically generating
symbols that cannot collide with variable names and requiring that all
metavariables be explicitly declared.

\subsection{Examples}
\label{sec:macro-examples}

In this section, we discuss some examples of macros.

\paragraph{Unless}
One of the simplest macros that can be defined is the \texttt{unless} macro
(shown in \Cref{fig:unless}), which is the opposite of \texttt{if}: it executes
the body only if the condition is not satisfied.  Because it captures arbitrary
blocks for both its condition and body, \texttt{unless} must be defined as a
block macro.  It requires no additional metavariables beyond the blocks that it
captures, so we can omit that portion of the definition.  The body of the macro
transforms the \texttt{cond} and \texttt{body} blocks into the equivalent
\texttt{if} expression by unquoting them (with the \texttt{@} operator) in the
appropriate locations.

\begin{figure}[htbp]
\caption{The \texttt{unless} macro}
\label{fig:unless}
\begin{verbatim}
macro_block unless
  syntax $'unless' cond $'then' conseq $'end'
  transform
    if @cond then else @conseq end
end
\end{verbatim}
\end{figure}

\paragraph{List}

\Cref{fig:list-source} contains an example of a program that defines the
\texttt{list} macro, with syntax exactly like that of a Python list.  This macro
assumes that \texttt{=} is used for assignment and that the \texttt{<} and
\texttt{+} operators have been defined.  The syntax line defines a rule that
matches any number of comma-separated expressions, and stores those expressions
in the metavariable called \texttt{values}.  The body of the macro builds up a
dictionary mapping indices to the values in \texttt{values}, starting from index
0.  This requires two extra metavariables, \texttt{i} and \texttt{l}, to store
the current index and partial list, respectively.  At the end of the macro, we
evaluate the complete list as the ``return value'' of the macro.

\begin{figure}
\caption{A \texttt{list} macro}
\label{fig:list-source}
\begin{verbatim}
macro_expr list (i, l)
  syntax $'[' (values * $',') $']'
  transform
    @l = {}
    @i = 0
    while @i < size(@values) do
        @l = insert(@l, @i, @values[@i])
        @i = @i + 1
    end
    @l
end

L = [$'Hello', 2]
print($'2 =', L[1])
\end{verbatim}
\end{figure}


\section{Grammar Modification}
\label{sec:grammar-mods}

There are a number of ways the grammar can be modified.  Because the grammar is
a regular Python object, we have the freedom to make modifications to it at
runtime or at interpreter start time.  At runtime, we use the evaluator to add
macro rules to the parser.  At startup, we use a settings file to add operator
definitions and extra rules for other syntax, such as assignments.

\subsection{Runtime Modifications}

The only runtime modifications available are those performed by runtime macros.
A macro definition causes the interpreter to create new parser rules according
to the syntax pattern and add them to the front of the \texttt{expr\_types} or
\texttt{statement\_types} list, depending on what type of macro is being
defined.  This causes the expression and statement rules to try parsing macros
from newest to oldest before falling back to the expression and statement types
defined by the base language.

\subsection{Startup Modifications}
\label{sec:grammar-mods}

There are three kinds of modifications to the grammar that can happen at
startup: adding operators, changing the unquote operator symbol, and mutating
the \texttt{Grammar} object after creation.

\paragraph{Operators}
Operators, as they relate to the grammar, are represented by a symbol, a
precedence, and (in the case of binary operators) an associativity.  We store
these properties (along with an evaluation function for the evaluator) in the
settings file.  As a convention, operator precedence is defined as an integer,
where an operator that binds more tightly has a higher precedence.

As shown in \Cref{fig:ops}, a binary operator definition consists of four parts:
the precedence level, the operator symbol, the associativity, and the evaluation
function.  A unary operator is defined in the same way, except that an
associativity is not needed.  We assume that all unary operators are prefix
operators.

\begin{figure}[htbp]
\caption{Defining operators in the settings file}
\label{fig:ops}
\begin{verbatim}
settings = {
    'binary_operations': [
        (2, '+', 'left', lambda l,r: l+r),    # binary addition
        (2, '-', 'left', lambda l,r: l-r),    # binary subtraction
        (3, '*', 'left', lambda l,r: l*r),    # binary multiplication
        (3, '/', 'left', lambda l,r: l/r),    # binary division
    ],
    'unary_operations': [
        (1, '~', lambda x: not x),            # unary not
    ],
}
\end{verbatim}
\end{figure}

\paragraph{Unquote}
Some users and instructors may prefer to use a symbol other than \texttt{@} as
their unquote operator.  A settings file defined as in \Cref{fig:unquote-op}
will use a two caret symbols (\texttt{\^{}\^{}}) as unquote.  This string will
automatically be added to the set of reserved substrings.

\begin{figure}[htbp]
\caption{Setting the unquote operator in the settings file}
\label{fig:unquote-op}
\begin{verbatim}
settings = {
    'unquote_operator': '^^',
}
\end{verbatim}
\end{figure}

\paragraph{Mutations}
The settings file can also define a list of hooks that will be run at the end of
the grammar definition.  An example of such a hook is included in the settings
file in \Cref{app:settings}.

This settings file uses a hook to define the missing syntax rules needed to
support declaration and assignment.  It first defines the rules inserts those
rules at the beginning of the \texttt{statement\_types} list.  The special
symbols used are then added to the \texttt{reserved\_substrings} set to keep
them from being parsed as identifiers.

The possible modifications are not limited to assignment rules.  The grammar
hooks are passed the entire grammar object as an argument, so these hooks can be
used to remove rules, reorder precedence in expression or statement types,
change the list of reserved words, add literals, or change anything else about
the parser.
