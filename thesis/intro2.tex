\chapter{Introduction}

Our goal for this project was to design a programming language that can be used
as a pedagogical tool for teaching introductory programming students.  Our
primary focus was in making a language with a simple notional machine
\cite{notional-machine} (for novice programmers) and powerful extensibility
features (for experienced programmers).  The language is aimed at late high
school or early undergraduate students.

Some general-purpose languages are also used as introductory languages (Python,
Java, and C++) \cite{intro-languages}.  These languages are powerful,
feature-filled, and contain many shortcuts for common tasks, which are all
benefits that come at the expense of complicated syntax or notional machines, so
subsets of these languages are usually taught in their place.  Students often
search the web for help in completing assignments, but the world outside the
teaching environment does not limit itself to the same subset of the language,
so students may not be able to grasp the answers they find online, for example.
Or, worse, the answer to their question may end up being a simple call to a
standard library function, which trivializes the entire assignment!

Other languages languages explicitly designed for novices include Logo
\cite{logo}, PLT Scheme (now known as Racket) \cite{racket}, and Scratch
\cite{scratch}.  These languages are also powerful and feature-filled, but they
bear little resemblance to other languages used in introductory programming
courses.  These languages may be successful in being easy for novices to learn,
but students may struggle with the transition to a later programming course that
uses different language.  Within the same programming language family, some
concepts can be translated with simple syntax transformations or notional
machine analogies, but some of these teaching languages implement different
paradigms entirely, so knowledge of one of these languages may not transfer well
to another.

Our approach was to start with a very basic language (in features and syntax) by
default and allow the user to extend the syntax at runtime with automatically
hygienic macros, which cannot accidentally capture identifiers in the larger
program.  Macros are generally seen as an advanced programming concept, but part
of our extensibility goal was to be able to introduce the concept of a runtime
macro in a programmer's first language to enable them to make useful syntactic
abstractions.  We wanted students who learned this language to be able to easily
move into other common languages, so the syntax of the base language is similar
to Python or Ruby, and the extensibility features of the interpreter allow the
language to borrow syntax and features from other languages.

In this thesis, we will start by describing the base language syntax and
features (\Cref{chap:language}).  We then describe the interpreter framework: a
custom parser generator (\Cref{chap:generator}), the language grammar itself
(\Cref{chap:grammar}), and the evaluator (\Cref{chap:evaluator}).  We conclude
with some commentary of the process and future work (\Cref{chap:conclusion}).
