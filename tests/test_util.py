from src import util

def test_namespace():
    def mkns():
        x = 10
        y = []
        def get_y(): return y
        def get_x(): return x
        locals_ = locals()
        def get_locals(): return locals_
        return util.Namespace(locals())

    ns = mkns()
    assert ns.get_x() == 10
    assert ns.get_y() == []

    ns.y.append('foo')
    assert ns.get_y() == ['foo']

    ns.y.insert(0, 'bar')
    assert ns.get_y() == ['bar', 'foo']

    assert ns.__dict__ is ns.get_locals()

    ns['z'] = 11
    assert ns['z'] == 11
