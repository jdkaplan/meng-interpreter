import os

import pytest

from src import generator
from src.grammar import Grammar
from src.evaluator import Evaluator, Environment, BuiltinFunc
from src.interpreter import interpret, InterpreterError
from src.util import Node, get_settings

TEST_ROOT = os.path.join(os.path.dirname(__file__), 'test_programs')
SETTINGS_ROOT = 'settings'

def make_parser(matcher):
    return lambda source: next(generator.make_parser(matcher)(source, 0))

def pytest_generate_tests(metafunc):
    if not metafunc.cls: return

    paramslist = metafunc.cls.params[metafunc.function.__name__]
    argnames = ['input_file', 'output_file', 'config']

    argvals = []
    id_list = []
    for params in paramslist:
        test_dirs = params['test_dirs']
        for test_dir in test_dirs:
            for filename in os.listdir(os.path.join(TEST_ROOT, test_dir, 'input')):
                if filename[-1] == '~': continue
                p = {
                    'config': params['config'],
                    'input_file': os.path.join(TEST_ROOT, test_dir, 'input', filename),
                    'output_file': os.path.join(TEST_ROOT, test_dir, 'output', filename),
                }
                argvals.append(p)
                id_list.append('{}/{}'.format(test_dir, filename))

    metafunc.parametrize(argnames, [[av[name] for name in argnames] for av in argvals], ids=id_list)

@pytest.fixture()
def config(request):
    return request.param

@pytest.fixture()
def input_file(request):
    return request.param

@pytest.fixture()
def output_file(request):
    return request.param

@pytest.fixture()
def settings(config):
    return get_settings([os.path.join(SETTINGS_ROOT, settings)
            for settings in config['settings_files']])

@pytest.fixture()
def evaluator(grammar, settings):
    return Evaluator(grammar, settings)

@pytest.fixture()
def grammar(settings):
    return Grammar(settings)

class Printer:
    def __init__(self):
        self.output = ''

    def print(self, *args):
        self.output += ' '.join(map(str, args)) + '\n'

@pytest.fixture()
def printer():
    return Printer()

@pytest.fixture()
def environment(evaluator, printer):
    return Environment(evaluator.base_environment, variables={
            'print': BuiltinFunc(printer.print),
    })

class TestEvaluator:
    params = {
        'test_evaluator': [
            {'test_dirs': ['default'], 'config': {'settings_files': []}},
            {'test_dirs': ['default', 'basic'], 'config': {'settings_files': ['basic.py']}},
            {'test_dirs': ['default', 'basic', '6.s080'], 'config': {'settings_files': ['6.s080.py']}},
            {'test_dirs': ['strict'], 'config': {'settings_files': ['strict.py']}},
        ],
    }

    def test_evaluator(self, input_file, output_file, evaluator, environment, grammar, printer):
        with open(output_file) as output:
            expected = output.read()
        with open(input_file) as source:
            src = source.read()

        if input_file.endswith('__error__'):
            try:
                for _ in interpret(src, evaluator, environment, grammar): pass
            except InterpreterError as e:
                assert str(e).strip() == expected.strip()
        else:
            for _ in interpret(src, evaluator, environment, grammar): pass
            assert printer.output == expected
