import os
from textwrap import dedent

import pytest

from src import generator
from src.grammar import Grammar
from src.util import Node, get_settings

SETTINGS_ROOT = 'settings'

def pytest_generate_tests(metafunc):
    paramslist = metafunc.cls.params
    argnames = ['config']
    argvalues = [[av[name] for name in argnames] for av in paramslist]
    metafunc.parametrize(argnames, argvalues)

@pytest.fixture()
def config(request):
    return request.param

@pytest.fixture()
def settings(config):
    return get_settings([os.path.join(SETTINGS_ROOT, settings)
            for settings in config['settings_files']])

@pytest.fixture()
def grammar(settings):
    return Grammar(settings)

def make_parser(matcher):
    return lambda source: next(generator.make_parser(matcher)(source, 0))

class Parser:
    def __init__(self, rule):
        self.rule = rule

    def __enter__(self):
        def parse(source):
            try:
                return next(generator.parse(self.rule, source, 0))
            except StopIteration:
                return None
        return parse

    def __exit__(self, exc_type, exc_value, traceback):
        pass

class TestDefault:
    params = [
        {'config': {'settings_files': []}},
        {'config': {'settings_files': ['basic.py']}},
    ]

    def test_integer(self, grammar):
        rule = grammar.integer
        with Parser(rule) as parse:
            assert parse('1')
            assert parse('12')
            assert not parse(' 12')
            assert not parse('\n12')
            assert parse('12 3')
            assert not parse('ab 12')

    def test_decimal(self, grammar):
        rule = grammar.decimal
        with Parser(rule) as parse:
            assert parse('1.0')
            assert parse('1.22')
            assert not parse(' 1.2')
            assert not parse('\n1.2')
            assert parse('1.2 3')
            assert not parse('ab 1.2')
            assert not parse('12')

    def test_string(self, grammar):
        rule = grammar.string
        with Parser(rule) as parse:
            content = lambda x: parse(x).data[0]
            assert content('$|source|') == 'source'
            assert content(r'$|sou\|rce|') == r'sou|rce'
            assert content(r'$|sou\nrce|') == 'sou\nrce'
            assert content('$|sou\nrce|') == 'sou\nrce'
            assert content(r'$|sou\|\n\rce|') == 'sou|\n\rce'
            assert content(r'$|sou\||\n\ce|') == 'sou|'
            assert not parse(r'$|sou\|\n\ce|')
            assert not parse(r'$"esc\'ape"')
            assert not parse(r'$"esc\xape"')

    def test_number(self, grammar):
        rule = grammar.number
        with Parser(rule) as parse:
            assert parse('1')
            assert parse('12')
            assert parse('1.2')
            assert not parse('ab 1')

    def test_literal(self, grammar):
        rule = grammar.literal
        with Parser(rule) as parse:
            assert parse('True')
            assert parse('False')
            assert parse('Nothing')
            assert not parse('ab 1')

    def test_dictionary(self, grammar):
        rule = grammar.dictionary
        with Parser(rule) as parse:
            assert parse('{}')
            assert parse('{0:0}')
            assert parse('{1:4,2:8}')
            assert [(k[0][0][0], v[0][0][0]) for (k,v) in parse('{1:4,2:8}').data] == [('1', '4'), ('2', '8')]

    def test_expr(self, grammar):
        rule = grammar.expr
        with Parser(rule) as parse:
            assert parse('1')
            assert parse('12')
            assert parse(' 12')
            assert not parse('\n12')
            assert parse('12 3')
            assert parse('(12) 3')
            assert not parse('if 12')
            assert parse('ab 12')

            assert parse('1.0')
            assert parse('1.22')
            assert parse(' 1.2')
            assert not parse('\n1.2')
            assert parse('1.2 3')
            assert parse('ab 1.2')

            assert parse('$|source|')
            assert parse(r'$|sou\|rce|')
            assert parse(r'$|sou\nrce|')
            assert parse('$|sou\nrce|')
            assert parse(r'$|sou\|\n\rce|')
            assert not parse(r'$|sou\|\n\ce|')
            assert parse(r'$|sou\||\n\ce|')

            assert parse('f()')
            assert parse('f(x)')
            assert parse('f(a,b)')
            assert parse('f(a,b,)')

            assert parse('d[]')
            assert parse('d[x]')
            assert parse('d[f(x)]')
            assert parse('d[f(x)]()')
            assert parse('d[f(x)]()').text == 'd[f(x)]()'

            assert parse('@id')

            assert parse(dedent('''\
            foo
            (bar)
            ''')).text == 'foo'

    def test_statement(self, grammar):
        rule = grammar.statement
        with Parser(rule) as parse:
            assert parse('1\n')
            assert parse('1')
            assert parse('$|source|')

    def test_block(self, grammar):
        rule = grammar.block
        with Parser(rule) as parse:
            assert parse('')
            assert parse('1\n2\n')
            assert parse('1\n\n2')
            assert len(parse('1 2').data.children) == 1
            assert isinstance(parse('1 2').data[0], Node)

            src = '''\
            otherthing
            x'''
            assert parse(src).text == src

            src = '''\
            x
            f(y)'''
            assert parse(src).text == src

            src = dedent('''\
            four
            lines
            of
            statements
            ''')
            assert parse(src).text == src


    def test_identifier(self, grammar):
        rule = grammar.identifier
        valid_identifiers = [
            'x',
            'abc123',
            "f'",
            # 'sla/shy',
            'snake_case',
            # 'hyphen-ated',
            '_sneaky',
            'mutating!',
            'predicate?',
        ]

        with Parser(rule) as parse:
            for name in valid_identifiers:
                assert parse(name).text == name

            assert not parse('(y')
            assert parse('x(y)').text == 'x'
            assert not parse('True(y)')

            for word in grammar.reserved_words:
                assert not parse(word)

    def test_function(self, grammar):
        rule = grammar.function
        with Parser(rule) as parse:
            assert parse('function(a,b)end')
            (params, body) = parse('function(a,b)end').data
            assert len(params) == 2
            assert len(body.children) == 0

            assert parse('function(a, b)end')

    def test_if(self, grammar):
        rule = grammar.if_statement
        with Parser(rule) as parse:
            assert parse('if cond then thing else otherthing end')
            assert parse('if cond then thing end')
            assert parse('if cond then else end')
            assert parse('if cond then else otherthing end')
            assert parse(dedent('''\
            if
                cond
            then
                thing
            else
                otherthing
            end
            '''))

    def test_macro(self, grammar):
        rule = grammar.macro
        with Parser(rule) as parse:
            assert parse(dedent('''\
            macro_expr list
                syntax $'[' (values * $',') $']'
                transform
                    @values
            end
            '''))

            assert parse('macro_block blk (x, y) syntax $"foo" transform end')

    def test_nested_spec_repeat(self, grammar):
        rule = grammar.macro
        with Parser(rule) as parse:
            assert parse(dedent('''\
            macro_block switch (i, out, found)
                syntax $'switch' value ($'case' cases $'do' bodies *) ($'default' default ?) $'end'
                transform
            end
            '''))

    def test_ws_one_line(self, grammar):
        rule = grammar.ws_one_line
        with Parser(rule) as parse:
            assert parse('\n').text == ''

class TestBasic:
    params = [
        {'config': {'settings_files': ['basic.py']}},
    ]

    def test_unary_minus(self, grammar):
        rule = grammar.U_Exp
        with Parser(rule) as parse:
            def all_parsed(text):
                assert parse(text).text == text
                return True
            assert(all_parsed('-1'))
            assert(all_parsed('-f()'))

    def test_binary_plus(self, grammar):
        rule = grammar.E
        with Parser(rule) as parse:
            def all_parsed(text):
                assert parse(text).text == text
                return True
            assert(all_parsed('1+1'))
            assert(all_parsed('f()+2'))
            assert(all_parsed('f()+g()'))

    def test_exp(self, grammar):
        rule = grammar.E
        with Parser(rule) as parse:
            def all_parsed(text):
                assert parse(text).text == text
                return True

            assert(all_parsed('-1'))
            assert(all_parsed('-f()'))

            assert(all_parsed('1+1'))
            assert(all_parsed('f()+2'))
            assert(all_parsed('f()+g()'))
            assert(all_parsed('7+8*2'))
            assert(all_parsed('7*8+2'))

            assert(all_parsed('(modulo(10, 3) == 0) == (modulo(n, 5) == 0)'))

    def test_if(self, grammar):
        rule = grammar.if_statement
        with Parser(rule) as parse:
            assert parse(dedent('''\
            if
                x = True
            then
                thing
            end
            '''))


            assert parse(dedent('''\
            if
                x = True
                x
            then
                thing
                x = False
            else
                otherthing
                x
            end
            '''))



    def test_statement(self, grammar):
        rule = grammar.statement
        with Parser(rule) as parse:
            def all_parsed(text):
                assert parse(text).text == text
                return True

            assert all_parsed('print(i, ks[i], d[ks[i]])')
            assert all_parsed('i = i + 1')
            assert all_parsed('i + 2*3 - 1')
            assert all_parsed('length(s1)-1')
            assert all_parsed('length(s1) - 1')
            assert all_parsed('print(characterAt(s1, length(s1) - 1))')

            assert all_parsed('(function() 0 end)')
            assert all_parsed('(function() 0 end)()')

            assert all_parsed('(@func)')
            assert all_parsed('(@func)()')


    def test_block(self, grammar):
        rule = grammar.block
        with Parser(rule) as parse:
            def all_parsed(text):
                assert parse(text).text == text
                return True

            src = '''\
            x = True
            x'''
            assert parse(src).text == src

        source = dedent('''\
        print(i, ks[i], d[ks[i]])
        i+1
        i = i + 1
        ''')
        assert all_parsed(source)

        source = dedent('''\
        if i < size(ks) then
            print(i, ks[i], d[ks[i]])
            i = i + 1
        end
        ''')
        assert all_parsed(source)

    def test_dicts(self, grammar):
        rule = grammar.block
        with Parser(rule) as parse:
            def all_parsed(text):
                assert parse(text).text == text
                return True

            source = dedent('''\
            if i < size(ks) then
                print(i, ks[i], d[ks[i]])
                i = i + 1
            end
            ''')
            assert all_parsed(source)

            source = dedent('''\
            d = {1: 2, $"foo": $"bar"}

            print(contains(d, 1))
            print(contains(d, 2))
            print(contains(d, $'foo'))
            print(contains(d, $"bar"))

            print(insert(d, True, Nothing))
            print(d)

            print(remove(d, $|foo|))
            print(d)

            i = 0
            ks = keys(d)
            if i < size(ks) then
                print(i, ks[i], d[ks[i]])
                i = i + 1
            end
            ''')
            assert all_parsed(source)

    def test_assignment(self, grammar):
        rule = grammar.assignment
        with Parser(rule) as parse:
            assert parse('x=1')
            assert parse('x=x')
            assert parse('x=True')
            assert not parse('True=1')
            assert not parse('True=x')
            assert not parse('True=True')

    def test_rainfall(self, grammar):
        rule = grammar.block
        with Parser(rule) as parse:
            def all_parsed(text):
                assert parse(text).text == text
                return True

    def test_unquote(self, grammar):
        rule = grammar.statement
        with Parser(rule) as parse:
            def all_parsed(text):
                assert parse(text).text == text
                return True

            def tree_equality(source, expected):
                actual = parse(source).data
                assert repr(actual) == repr(expected)
                return True

            source = '@i < size(@y)'
            assert all_parsed(source)
            _ = Node
            assert tree_equality(source,
                _('statement',
                    _('expr',
                        _('binary_operation', '<',
                            _('expr', _('unquote', _('expr', _('identifier', 'i')))),
                            _('expr', _('call', _('expr', _('identifier', 'size')),
                                    _('expr', _('unquote', _('expr', _('identifier', 'y')))),
                            ))
                        )
                    )
                )
            )

    def test_expr(self, grammar):
        rule = grammar.statement
        with Parser(rule) as parse:
            def tree_equality(source, expected):
                actual = parse(source).data
                assert repr(actual) == repr(expected)
                return True
            _ = Node

            source = dedent('''\
            dy = y1 - y2
            (dx^2 + dy^2)^0.5
            ''')
            assert parse(source).text == 'dy = y1 - y2'
