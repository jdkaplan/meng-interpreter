from textwrap import dedent

import pytest

from src import generator
from src.grammar import Grammar
from src.evaluator import make_syntax_rule
from src.util import Node, get_settings

def make_parser(matcher):
    return lambda source: next(generator.make_parser(matcher)(source, 0))

@pytest.fixture()
def config():
    return {'settings_files': []}

@pytest.fixture()
def settings(config):
    return get_settings([os.path.join(SETTINGS_ROOT, settings)
            for settings in config['settings_files']])

@pytest.fixture()
def grammar(settings):
    return Grammar(settings)

def test_make_syntax_rule(grammar):
    ast = Node('string', 'switch', "'")
    rule = make_syntax_rule(ast, grammar.expr, grammar)
    parser = make_parser(rule)
    assert parser('switch')

    ast = Node('identifier', 'value')
    rule = make_syntax_rule(ast, grammar.expr, grammar)
    parser = make_parser(rule)
    assert parser('someid9').text == 'someid9'

    ast = [([Node('string', 'case', "'"), Node('identifier', 'cases'), Node('string', 'do', "'"), Node('identifier', 'bodies')], '*', None)]
    rule = make_syntax_rule(ast, grammar.expr, grammar)
    parser = make_parser(rule)
    assert parser('')
    assert parser('case 1 do something')
    assert parser('case 1 do something case 2 do something_else')

def test_make_syntax_rule_2(grammar):
    ast = [Node('string', 'switch', "'"), Node('identifier', 'value'), ([Node('string', 'case', "'"), Node('identifier', 'cases'), Node('string', 'do', "'"), Node('identifier', 'bodies')], '*', None), ([Node('string', 'default', "'"), Node('identifier', 'default')], '?', None), Node('string', 'end', "'")]
    rule = make_syntax_rule(ast, grammar.block, grammar)
    parser = make_parser(rule)
    assert parser('switch v end')
    assert parser('switch v default print(Nothing) end')
    assert parser('switch v case 1 do something end')
    assert parser('switch v case 1 do something\ncase 2 do some_other_thing end')
    assert parser('switch v case 1 do something\ncase 2 do some_other_thing\ndefault print(Nothing) end')

def test_abs(grammar):
    syntax = next(grammar.macro_syntax(dedent('''\
    syntax $'|' x $'|'
    '''), 0)).data
    assert syntax
    rule = make_syntax_rule(syntax, grammar.expr, grammar)
    assert next(rule('| foo |', 0))

    # # These would require changes to how identifiers are found or
    # # how macros are defined.  We could probably heuristically
    # # determine if we need to add to reserved_words or
    # # reserved_substrings based on alphanumerics, but I don't feel comfortable with that
    # assert next(rule('|foo |', 0))
    # assert next(rule('| foo|', 0))
    # assert next(rule('|foo|', 0))
