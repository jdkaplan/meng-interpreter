from src import generator

from src.generator import *

def count(iterable): return len(list(iterable))

class Parser:
    def __init__(self, rule):
        self.rule = rule

    def __enter__(self):
        return lambda source: generator.parse(self.rule, source, 0)

    def __exit__(self, exc_type, exc_value, traceback):
        pass

word = regex('\w+')

def test_raw():
    rule = raw('foo')
    with Parser(rule) as parse:
        assert count(parse('foo')) == 1
        assert count(parse('bar')) == 0

def test_regex():
    rule = regex(r'a*')
    with Parser(rule) as parse:
        assert count(parse('')) == 1
        assert count(parse('a')) == 1
        assert count(parse('aa')) == 1

        assert count(parse('b')) == 1
        assert next(parse('b')).text == ''

def test_choice():
    rule = choice([raw('foo'), raw('bar'), regex(r'.*')])
    with Parser(rule) as parse:
        assert count(parse('foo')) == 2
        assert count(parse('bar')) == 2
        assert count(parse('thing')) == 1

def test_sequence():
    rule = sequence([regex(r'a*'), regex(r'b')])
    with Parser(rule) as parse:
        assert count(parse('b')) == 1
        assert count(parse('ab')) == 1
        assert count(parse('aab')) == 1
        assert [re_matched(m) for m in next(parse('aab')).data] == ['aa', 'b']
        assert next(parse('aa b')).text == 'aa b'
        assert [re_matched(m) for m in next(parse('aa b')).data] == ['aa', 'b']
        assert count(parse('a')) == 0

    rule = sequence([raw(r' '), raw('\n')], skip_whitespace=raw(''))
    with Parser(rule) as parse:
        assert count(parse(' \n')) == 1

    ws_all_star = regex(r'\s*')
    def ws_one_line(source, position):
        matcher = ws_all_star
        for match in generator.parse(matcher, source, position):
            text = match.text
            ws, *_ = re.split(r'\n', text)
            yield Match(ws, position, ws)

    rule = sequence([word, word], skip_whitespace=ws_one_line)
    with Parser(rule) as parse:
        assert count(parse('foo\nbar')) == 0

    rule = sequence([word, word], skip_whitespace=ws_all_star)
    with Parser(rule) as parse:
        assert count(parse('foo\nbar')) == 1

def test_optional_separator():
    rule = sequence([
            raw('foo'),
            optional(raw(':')),
            choice([
                    regex(r'\d+'),
                    regex(r'\w+'),
                    regex(r'.+'),
            ]),
    ])
    with Parser(rule) as parse:
        assert count(parse('foo:19')) == 4
        assert count(parse('foo:bar')) == 3

def test_star():
    rule = star(raw('a'))
    with Parser(rule) as parse:
        assert [m.text for m in parse('a')] == ['a', '']
        assert [m.text for m in parse('aa')] == ['aa', 'a', '']

    rule = star(choice([raw('a'), raw('b')]))
    with Parser(rule) as parse:
        assert [m.text for m in parse('a')] == ['a', '']
        assert [m.text for m in parse('b')] == ['b', '']
        assert [m.text for m in parse('ab')] == ['ab', 'a', '']
        assert [m.text for m in parse('abb')] == ['abb', 'ab', 'a', '']
        assert [m.text for m in parse('abba')] == ['abba', 'abb', 'ab', 'a', '']

def test_plus():
    rule = plus(raw('a'))
    with Parser(rule) as parse:
        assert [m.text for m in parse('a')] == ['a']
        assert [m.text for m in parse('aa')] == ['aa', 'a']

    rule = plus(choice([raw('a'), raw('b')]))
    with Parser(rule) as parse:
        assert [m.text for m in parse('a')] == ['a']
        assert [m.text for m in parse('b')] == ['b']
        assert [m.text for m in parse('ab')] == ['ab', 'a']
        assert [m.text for m in parse('abb')] == ['abb', 'ab', 'a']
        assert [m.text for m in parse('abba')] == ['abba', 'abb', 'ab', 'a']

def test_separated_star():
    rule = separated_star(raw('x'), raw(','))
    with Parser(rule) as parse:
        assert [m.text for m in parse('')] == ['']
        assert [m.text for m in parse('x,')] == ['x,', 'x', '']
        assert [len(m.data) for m in parse('x,')] == [1, 1, 0]
        assert [m.text for m in parse('x,x,')] == ['x,x,', 'x,x', 'x,', 'x', '']
        assert [len(m.data) for m in parse('x,x,')] == [2, 2, 1, 1, 0]

    rule = separated_star(raw('x'), raw(','), keep_separators=True)
    with Parser(rule) as parse:
        assert [m.text for m in parse('x,')] == ['x,', 'x', '']
        assert [len(m.data) for m in parse('x,')] == [2, 1, 0]

    rule = separated_star(regex(r'\w+'), regex(r'\s*\n+\s*'))
    with Parser(rule) as parse:
        assert [m.text for m in parse('foo \n bar')] == ['foo \n bar', 'foo \n ', 'foo', '']

    rule = separated_star(
        regex('\S+'),
        regex('\n+'),
        skip_whitespace=regex(r' *')
    )
    with Parser(rule) as parse:
        assert [m.text for m in parse('foo \n bar')] == ['foo \n bar', 'foo \n', 'foo', '']


def test_separated_plus():
    rule = separated_plus(raw('x'), raw(','))
    with Parser(rule) as parse:
        assert count(parse('')) == 0
        assert count(parse('x')) == 1
        assert count(parse('x,')) == 2
        assert count(parse('x , x')) == 3
        assert count(parse('x,x,x,'))

        assert [m.text for m in parse('')] == []
        assert [m.text for m in parse('x,')] == ['x,', 'x']
        assert [len(m.data) for m in parse('x,')] == [1, 1]
        assert [m.text for m in parse('x,x,')] == ['x,x,', 'x,x', 'x,', 'x']
        assert [len(m.data) for m in parse('x,x,')] == [2, 2, 1, 1]

    rule = separated_plus(raw('x'), raw(','), keep_separators=True)
    with Parser(rule) as parse:
        assert [m.data for m in parse('x,x,')] == [
            ['x', ',', 'x', ','],
            ['x', ',', 'x', None],
            ['x', ','],
            ['x', None],
        ]
