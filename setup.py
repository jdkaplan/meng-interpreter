import os
from setuptools import setup

requires = ()

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "lang",
    version = "0.0.0",
    author = "Jeremy Kaplan",
    author_email = "jdkaplan@mit.edu",
    description = ("jdkaplan's M.Eng"),
    license = "Unlicensed",
    keywords = "",
    url = "http://bitbucket.org/jdkaplan/meng-interpreter",
    packages=['src',],
    # namespace_packages = ['package_name'],
    install_requires=requires,
    long_description=read('README.md'),
    classifiers=[],
)
