from src.util import Node
import src.generator as generator

def add_assignment(grammar):
    def assignment(source, position):
        matcher = generator.sequence([
                generator.choice(grammar.assignable),
                generator.raw('='),
                grammar.expr,
        ], skip_whitespace=grammar.ws_one_line, callback=lambda seq: Node('assignment', seq[0], seq[2]))
        yield from generator.parse(matcher, source, position)

    def declaration(source, position):
        matcher = generator.sequence([
                generator.raw('var'),
                generator.choice(grammar.assignable),
                generator.raw('='),
                grammar.expr,
        ], skip_whitespace=grammar.ws_one_line, callback=lambda seq: Node('declare_mutable', seq[1], seq[3]))
        yield from generator.parse(matcher, source, position)

    grammar.assignable = [grammar.identifier, grammar.unquote]

    grammar.assignment = assignment
    grammar.statement_types.insert(0, assignment)

    grammar.declaration = declaration
    grammar.statement_types.insert(0, declaration)

    grammar.reserved_words |= set(['var'])
    grammar.reserved_substrings |= set(['='])

settings = {
    'declaration_required': True,
    'grammar_post_create': [add_assignment],
    'binary_operations': [
        (0, '==', 'left', lambda l,r: l==r),
        (0, '!=', 'left', lambda l,r: l!=r),
        (1, '>=', 'left', lambda l,r: l>=r),
        (1, '<=', 'left', lambda l,r: l<=r),
        (1, '>', 'left', lambda l,r: l>r),
        (1, '<', 'left', lambda l,r: l<r),
        (2, '+', 'left', lambda l,r: l+r),
        (2, '-', 'left', lambda l,r: l-r),
        (3, '*', 'left', lambda l,r: l*r),
        (3, '/', 'left', lambda l,r: l/r),
    ],
    'unary_operations': [
        (2, '-', lambda x: -x),
    ],
}
