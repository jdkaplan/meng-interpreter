settings = {
    'unary_operations': [
        (1, 'not', 'right', lambda b: not b),
    ],
    'binary_operations': [
        (4, '++', lambda l,r: l+r),
    ],
}
