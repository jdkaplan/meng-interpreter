from src import generator
from src.util import Node

def add_assignment(grammar):
    def assignment(source, position):
        matcher = generator.sequence([
                generator.choice(grammar.assignable),
                generator.raw('='),
                grammar.expr,
        ], skip_whitespace=grammar.ws_one_line, callback=lambda seq: Node('assignment', seq[0], seq[2]))
        yield from generator.parse(matcher, source, position)

    def declare_mutable(source, position):
        matcher = generator.sequence([
                generator.choice(grammar.assignable),
                generator.raw(':='),
                grammar.expr,
        ], skip_whitespace=grammar.ws_one_line, callback=lambda seq: Node('declare_mutable', seq[0], seq[2]))
        yield from generator.parse(matcher, source, position)

    def declare_immutable(source, position):
        matcher = generator.sequence([
                generator.choice(grammar.assignable),
                generator.raw('::='),
                grammar.expr,
        ], skip_whitespace=grammar.ws_one_line, callback=lambda seq: Node('declare_immutable', seq[0], seq[2]))
        yield from generator.parse(matcher, source, position)

    grammar.assignable = [grammar.identifier, grammar.unquote]

    grammar.assignment = assignment
    grammar.statement_types.insert(0, assignment)

    grammar.declare_mutable = declare_mutable
    grammar.statement_types.insert(0, declare_mutable)

    grammar.declare_immutable = declare_immutable
    grammar.statement_types.insert(0, declare_immutable)

    grammar.reserved_substrings |= set(['=', ':=', '::='])

settings = {
    'binary_operations': [
        (0, '==', 'left', lambda l,r: l==r),
        (0, '!=', 'left', lambda l,r: l!=r),
        (1, '>=', 'left', lambda l,r: l>=r),
        (1, '<=', 'left', lambda l,r: l<=r),
        (1, '>', 'left', lambda l,r: l>r),
        (1, '<', 'left', lambda l,r: l<r),
        (2, '+', 'left', lambda l,r: l+r),
        (2, '-', 'left', lambda l,r: l-r),
        (3, '*', 'left', lambda l,r: l*r),
        (3, '/', 'left', lambda l,r: l/r),
    ],
    'unary_operations': [
        (2, '-', lambda x: -x),
    ],
    'grammar_post_create': [add_assignment],
}
