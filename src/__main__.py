import argparse

from .interpreter import *

argparser = argparse.ArgumentParser()
argparser.add_argument('-i', '--interactive', action='store_true')
argparser.add_argument('-s', '--settings', action='append')
argparser.add_argument('files', nargs='*')

args = vars(argparser.parse_args())
files = args['files']
interactive = args['interactive'] or not files
settings = util.get_settings(args['settings'] or [])

grammar = Grammar(settings)
evaluator = Evaluator(grammar, settings)
environment = Environment(evaluator.base_environment)

for filename in files:
    with open(filename) as f:
        source = f.read()

        for value in interpret(source, evaluator, environment, grammar):
            pass

if interactive:
    repl(evaluator, environment, grammar, settings)
