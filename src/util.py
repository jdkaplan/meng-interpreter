import copy
import importlib
import inspect

from . import generator

class Node:
    def __init__(self, type, *children):
        self.type = type
        self.children = list(children)

    def __repr__(self):
        return "({})".format(' '.join([str(self.type)] + list(map(str, self.children))))
        return "Node({})".format(', '.join([repr(self.type)] + list(map(repr, self.children))))

    def __getitem__(self, key):
        return self.children[key]

class Namespace:
    def __init__(self, kwargs):
        self.__dict__ = kwargs

    def __repr__(self):
        keys = sorted(self.__dict__)
        items = ("{}={!r}".format(k, self.__dict__[k]) for k in keys)
        return "{}({})".format(type(self).__name__, ", ".join(items))

    def __getitem__(self, key):
        return self.__dict__[key]

    def __setitem__(self, key, val):
        self.__dict__[key] = val


class Operation: pass

class BinaryOperation(Operation):
    def __init__(self, precedence, symbol, associativity, evaluator):
        self.precedence = precedence
        self.symbol = symbol
        self.associativity = associativity
        self.evaluator = evaluator

class UnaryOperation(Operation):
    def __init__(self, precedence, symbol, evaluator):
        self.precedence = precedence
        self.symbol = symbol
        self.evaluator = evaluator

def dict_join(*dicts):
    result = {}
    for d in dicts:
        for key in d:
            if key not in result:
                result[key] = copy.deepcopy(d[key])
            elif isinstance(result[key], dict) and isinstance(d[key], dict):
                result[key] = dict_join(result[key], d[key])
            elif isinstance(result[key], list) and isinstance(d[key], list):
                result[key] += d[key]
            else:
                result[key] = copy.deepcopy(d[key])
    return result

join_settings = dict_join

DefaultSettings = {
    'partial_application': False,
    'redeclaration_is_an_error': True,
    'canonical_string_delimiter': None,
    'immutable_function_arguments': False,
    'declaration_required': False,
    'binary_operations': [],
    'unary_operations': [],
    'grammar_post_create': [],
    'unquote_operator': '@',
}

def get_settings(filenames):
    result = copy.deepcopy(DefaultSettings)
    for filename in filenames:
        module = importlib.machinery.SourceFileLoader(filename, filename).load_module()
        result = join_settings(result, module.settings)
    return result


class Macro():
    def __init__(self, variables, body):
        self.variables = variables
        self.body = body
