from itertools import chain, repeat, takewhile
import re

## Utilities

def identity(x): return x
def is_valid_match(match): return match is not None
def valid_matches(results): return takewhile(is_valid_match, results)
def re_matched(sre): return sre.string[sre.start():sre.end()]
def parse(rule, source, position):
    yield from valid_matches(make_parser(rule)(source, position))

## Data structures

class Match:
    def __init__(self, text, position, data):
        self.text = text
        self.position = position
        self.data = data

    def __repr__(self):
        return 'Match({}, {}, {})'.format(repr(self.text), self.position, repr(self.data))

    @classmethod
    def from_sre(cls, sre, position, data):
        return cls(re_matched(sre), position, data) if sre else None


def make_parser(rule):
    def parser(source, position):
        return chain(rule(source, position), repeat(None))
    return parser


## Combinators

def raw(string, callback=identity):
    def rule(source, position):
        src = source[position : position+len(string)]
        if src == string:
            yield Match(string, position, callback(src))
    return rule

def regex(pattern, callback=identity):
    def rule(source, position):
        sre = re.match(pattern, source[position:])
        if sre:
            yield Match.from_sre(sre, position, callback(sre))
    return rule

def optional(rule, callback=identity):
    def rule_optional(source, position):
        for match in parse(rule, source, position):
            yield Match(match.text, match.position, callback(match.data))
        yield Match('', position, callback(None))
    return rule_optional

def choice(rules, callback=identity):
    def rule(source, position):
        for option in rules:
            for match in parse(option, source, position):
                yield Match(match.text, match.position, callback(match.data))
    return rule

def sequence(rules, callback=identity, skip_whitespace=regex(r'\s*')):
    # necessary that `skip_whitespace` matches the empty string
    def helper(rules, result, source, position):
        if len(rules) == 0:
            yield result
        else:
            if skip_whitespace:
                ws_match = parse(skip_whitespace, source, position)
                position += len(next(ws_match).text)
            for match in parse(rules[0], source, position):
                tip = position + len(match.text)
                yield from helper(rules[1:], result + [match], source, tip)

    def rule(source, position):
        for result in helper(rules, [], source, position):
            tip = position
            if result:
                last = result[-1]
                tip = last.position + len(last.text)
            data = [m.data for m in result]
            yield Match(source[position:tip], position, callback(data))
    return rule

def star(rule, callback=identity):
    def helper(result, source, position):
        for match in parse(rule, source, position):
            tip = position + len(match.text)
            result = result + [match]
            yield from helper(result, source, tip)
            yield result

    def rule_star(source, position):
        for result in helper([], source, position):
            tip = position + sum(len(match.text) for match in result)
            data = [m.data for m in result]
            yield Match(source[position:tip], position, callback(data))
        yield Match('', position, callback([]))
    return rule_star

def plus(rule, callback=identity):
    def helper(result, source, position):
        for match in parse(rule, source, position):
            tip = position + len(match.text)
            result = result + [match]
            yield from helper(result, source, tip)
            yield result

    def rule_plus(source, position):
        for result in helper([], source, position):
            tip = position + sum(len(match.text) for match in result)
            yield Match(source[position:tip], position, callback(result))
    return rule_plus

def separated_star(rule, separator, callback=identity, keep_separators=False, skip_whitespace=None):
    # necessary that `skip_whitespace` matches the empty string
    def helper(result, source, position):
        if skip_whitespace:
            ws_match = parse(skip_whitespace, source, position)
            position += len(next(ws_match).text)
        for rule_match in parse(rule, source, position):
            res = result + [rule_match]
            tip = position + len(rule_match.text)
            if skip_whitespace:
                ws_match = parse(skip_whitespace, source, tip)
                tip += len(next(ws_match).text)
            for sep_match in parse(separator, source, tip):
                yield from helper(res + [sep_match], source, tip + len(sep_match.text))
            yield res
        yield result

    def rule_separated(source, position):
        for result in helper([], source, position):
            if result:
                last = result[-1]
                tip = last.position + len(last.text)
            else:
                tip = position
            if not keep_separators:
                result = result[::2]
            data = [m.data for m in result]
            yield Match(source[position:tip], position, callback(data))
    return rule_separated

def separated_plus(rule, separator, callback=identity, keep_separators=False):
    # rule (separator rule)* (separator?)?
    def cb_seq(seq):
        r, x, s = seq
        return callback([r] + x + ([s] if keep_separators else []))
    def cb_star(star):
        return [item for seq in star for item in seq]
    def cb_ins(ins):
        s, r = ins
        if keep_separators:
            return [s, r]
        return [r]

    return sequence([
            rule,
            star(sequence([separator, rule], callback=cb_ins), callback=cb_star),
            optional(separator),
    ], callback=cb_seq)

def single(rule, callback=identity):
    def rule_single(source, position):
        for match in parse(rule, source, position):
            yield Match(match.text, match.position, callback(match.data))
    return rule_single
