import collections
import copy
import itertools
import fractions
import functools
import types
from pprint import pprint

from .import util
from .import generator

class Error(Exception): pass
class RedeclarationError(Error): pass
class UndeclaredVariableError(Error): pass
class ImmutableAssignmentError(Error): pass
class IncorrectTypeError(Error): pass

DEBUG = False

class Environment:
    def __init__(self, parent, variables=None, settings=None):
        self.parent = parent
        self.settings = settings if settings != None else parent.settings
        self.variables = variables if variables != None else {}
        self.immutables = set()

    def __getitem__(self, key):
        if key in self.variables:
            return self.variables[key]
        elif self.parent != None:
            return self.parent[key]
        else:
            raise KeyError(key)

    def assign(self, key, val):
        if key in self.variables:
            if key in self.immutables:
                raise ImmutableAssignmentError('Assignment to immutable {}'.format(key))
            self.variables[key] = val
        elif not self.settings['declaration_required']:
            self.variables[key] = val
        elif self.parent != None:
            self.parent.assign(key, val)
        else:
            raise UndeclaredVariableError('Assignment to undeclared variable {}'.format(key))

    def declare_mutable(self, key, val):
        if key in self.variables and (self.settings['redeclaration_is_an_error'] or key in self.immutables):
            raise RedeclarationError('{} already declared'.format(key))
        self.variables[key] = val

    def declare_immutable(self, key, val):
        if key in self.variables:
            raise RedeclarationError('{} already declared'.format(key))
        self.variables[key] = val
        self.immutables.add(key)


# TODO: abc.ABCMeta and abc.abstractmethod
class Root: pass

class Nothing(Root):
    def __repr__(self):
        return 'Nothing'

    __str__ = __repr__

    def __hash__(self):
        return hash((None, None))

    def __eq__(self, other):
        return type(self) == type(other)

    def __ne__(self, other):
        return not self.__eq__(other)


@functools.total_ordering
class Boolean(Root):
    def __init__(self, truthiness):
        self.truthiness = truthiness

    def __repr__(self):
        return 'True' if self.truthiness else 'False'

    __str__ = __repr__

    def __hash__(self):
        return hash((None, self.truthiness))

    def __eq__(self, other):
        return type(self) == type(other) and self.truthiness == other.truthiness

    def __lt__(self, other):
        return type(self) == type(other) and (not self.truthiness and other.truthiness)

    def __bool__(self):
        return self.truthiness


class Rational(Root, fractions.Fraction):
    def __repr__(self):
        n, d = self.numerator, self.denominator
        if self.denominator == 1:
            return '{}'.format(n)
        return '{}/{}'.format(n, d)

    __str__ = __repr__

def as_decimal(rat):
    # TODO: Decimal won't work, float rounds badly, do the division myself?
    return str(float(rat))


class String(Root):
    def __init__(self, content, delimiter):
        self.content = content
        self.delimiter = delimiter

    def __repr__(self):
        return '${delimiter}{content}{delimiter}'.format(
            content = self.content.replace(self.delimiter, '\\'+self.delimiter),
            delimiter = self.delimiter,
        )

    def __str__(self):
        return self.content

    def __hash__(self):
        return hash(self.content)

    def __eq__(self, other):
        return type(self) == type(other) and self.content == other.content

    def __ne__(self, other):
        return not self.__eq__(other)

def string_length(string):
    if type(string) == String:
        return len(string.content)
    raise IncorrectTypeError('Expected a string, got {}'.format(string))

def string_concatenate(s1, s2):
    return String(s1.content + s2.content, s1.delimiter)

def string_character(string, index):
    return String(string.content[int(index)], string.delimiter)


class Func(Root):
    def __repr__(self):
        return '<function hash={:x}>'.format(hash(self))


@functools.total_ordering
class BuiltinFunc(Func):
    def __init__(self, function):
        self.function = function

    def call(self, arguments, top):
        return lift(self.function(*arguments))

    def __eq__(self, other):
        return self is other

    def __lt__(self, other):
        return type(self) == type(other) and hash(self) < hash(other) # pragma: no cover

    def __hash__(self):
        return hash(self.function)

@functools.total_ordering
class NativeFunc(Func):
    def __init__(self, params, body, parent_environment):
        self.params = params
        self.body = body
        self.parent_environment = parent_environment
        self._hash = hash((tuple(self.params), self.body, self.parent_environment))

    def call(self, args, top):
        if len(args) < len(self.params):
            if top.settings['partial_application']:
                curried_environment = Environment(self.parent_environment)

                if top.settings['immutable_function_arguments']:
                    declare = curried_environment.declare_immutable
                else:
                    declare = curried_environment.declare_immutable

                for (name,), arg in zip(self.params[:len(args)], args):
                    declare(name, arg)

                return NativeFunc(self.params[len(args):], self.body, curried_environment)
            else:
                raise Error('Too few arguments in function call'.format(self))

        if len(args) > len(self.params):
            raise Error('Too many arguments in function call'.format(self))

        environment = Environment(self.parent_environment)

        if top.settings['immutable_function_arguments']:
            declare = environment.declare_immutable
        else:
            declare = environment.declare_immutable

        for (name,), arg in zip(self.params, args):
            declare(name, arg)

        return top.evaluate(self.body, environment)

    def __eq__(self, other):
        return self is other

    def __lt__(self, other):
        return hash(self) < hash(other) # pragma: no cover

    def __hash__(self):
        return self._hash

class Dictionary(Root, collections.Mapping):
    def __init__(self, kvs):
        self.data = dict(kvs)

    def __getitem__(self, key):
        return self.data[key]

    def __iter__(self):
        return iter(self.data)

    def __len__(self):
        return len(self.data)

    def items(self):
        return sorted(self.data.items(), key=dictionary_sort_index)

    def __hash__(self):
        return hash(frozenset(self.items()))

    def __repr__(self):
        return '{{{}}}'.format(', '.join('{}: {}'.format(repr(key), repr(value)) for key, value in self.items()))

    __str__ = __repr__

    def __eq__(self, other):
        if type(self) == type(other):
            return frozenset(self.items()) == frozenset(other.items())
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

class LazyDictionary(Dictionary):
    def __init__(self, evaluate, kvs):
        super().__init__(kvs)
        self.evaluate = evaluate

    def __getitem__(self, key):
        return self.evaluate(super().__getitem__(key))


sort_order = [Nothing, Boolean, Rational, String, BuiltinFunc, NativeFunc, Dictionary]

def dictionary_sort_index(item):
    key, val = item
    return (sort_order.index(type(key)), key, val)

def dictionary_contains(dictionary, key):
    return key in dictionary

def dictionary_insert(dictionary, key, value):
    new_dictionary = {k: v for k, v in dictionary.items()}
    new_dictionary[key] = value
    return Dictionary(new_dictionary)

# TODO: is it possible to macro this to pretend mutability exists?
# ex: del!(dict, key)
def dictionary_remove(dictionary, key):
    new_dictionary = {k: v for k, v in dictionary.items() if k != key}
    return Dictionary(new_dictionary)

def dictionary_keys(dictionary):
    keys = sorted(dictionary.keys(), key=lambda k: (sort_order.index(type(k)), k))
    keys_as_dictionary = {i:k for i,k in enumerate(keys)}
    return lift(keys_as_dictionary)

def dictionary_size(dictionary):
    size = len(dictionary)
    return lift(size)

def lift(value):
    if isinstance(value, Root):
        return value
    return {
        type(None): lambda _: Nothing(),
        bool: Boolean,
        fractions.Fraction: Rational,
        int: Rational,
        str: lambda x: String(x, "'"),
        types.BuiltinFunctionType: BuiltinFunc,
        dict: lambda d: Dictionary({lift(k) : lift(v) for k,v in d.items()}),
        float: Rational,
    }[type(value)](value)

gensym = ('$macrosym_' + str(i) for i in itertools.count(0))

if DEBUG:
    debug = lambda *x: print('debug:', *x)
    pdebug = lambda x: (print('DEBUG:'), pprint(x))
else:
    debug = lambda *x: None
    pdebug = lambda x: None

# http://norvig.com/lispy2.html

class Evaluator:
    def __init__(self, grammar, settings=util.DefaultSettings):
        self.settings = settings
        self.base_environment = Environment(None, settings=settings, variables={
                'print': BuiltinFunc(print),

                'length': BuiltinFunc(string_length),
                'concatenate': BuiltinFunc(string_concatenate),
                'characterAt': BuiltinFunc(string_character),

                'contains': BuiltinFunc(dictionary_contains),
                'insert': BuiltinFunc(dictionary_insert),
                'remove': BuiltinFunc(dictionary_remove),
                'keys': BuiltinFunc(dictionary_keys),
                'size': BuiltinFunc(dictionary_size),

                'as_decimal': BuiltinFunc(as_decimal),
        })

        self.unary_operations = {}
        for params in settings['unary_operations']:
            op = util.UnaryOperation(*params)
            self.unary_operations[op.symbol] = op

        self.binary_operations = {}
        for params in settings['binary_operations']:
            op = util.BinaryOperation(*params)
            self.binary_operations[op.symbol] = op

        self.local_macros = {}
        self.grammar = grammar


    def evaluate(self, node, env):
        while node.type in {'expr', 'statement'}:
            node, = node

        if node.type == 'block':
            val = Nothing()
            for statement in node:
                val = self.evaluate(statement, env)
            return val

        # numbers
        if node.type == 'number':
            (number,), = node
            retval = Rational(number)
            return retval

        # literals
        # TODO: Node('boolean', True) and Node('boolean', False)
        if node.type == 'True':
            return Boolean(True)
        if node.type == 'False':
            return Boolean(False)
        if node.type == 'Nothing':
            return Nothing()

        # strings
        if node.type == 'string':
            content, delimiter = node
            delimiter = self.settings['canonical_string_delimiter'] or delimiter
            return String(content, delimiter)

        # dictionaries
        if node.type == 'dictionary':
            kvs = node
            dictionary = {}
            for k,v in kvs:
                key = self.evaluate(k, env)
                value = self.evaluate(v, env)
                dictionary[key] = value
            return Dictionary(dictionary)

        if node.type == 'access':
            dictionary, key = node
            d = self.evaluate(dictionary, env)
            k = self.evaluate(key, env)
            return d[k]

        # operators
        if node.type == 'binary_operation':
            symbol, left, right = node
            op = self.binary_operations[symbol]
            l = self.evaluate(left, env)
            r = self.evaluate(right, env)
            return lift(op.evaluator(l, r))
        if node.type == 'unary_operation':
            symbol, argument = node
            op = self.unary_operations[symbol]
            arg = self.evaluate(argument, env)
            return lift(op.evaluator(arg))


        # functions
        if node.type == 'function':
            params, body = node
            return NativeFunc(params, body, env)
        if node.type == 'call':
            callee, *arguments = node
            func = self.evaluate(callee, env)
            args = [self.evaluate(arg, env) for arg in arguments]
            return func.call(args, self)


        # environments
        if node.type == 'identifier':
            name, = node
            return env[name]

        # assignments
        if node.type in {'assignment', 'declare_mutable', 'declare_immutable'}:
            lhs, rhs = node
            if lhs.type == 'unquote':
                lhs = self.evaluate(lhs[0], env)[0]
            lhs, = lhs
            value = self.evaluate(rhs, env)
            action = {
                'assignment': env.assign,
                'declare_mutable': env.declare_mutable,
                'declare_immutable': env.declare_immutable,
            }[node.type]
            action(lhs, value)
            return value

        # control flow
        if node.type == 'if':
            condition, consequent, alternative = node
            cond = self.evaluate(condition, env)
            branch = consequent if cond else alternative
            return self.evaluate(branch, env)

        if node.type == 'while':
            condition, body = node
            retval = Nothing()
            while self.evaluate(condition, env):
                retval = self.evaluate(body, env)
            return retval

        # macros
        if node.type in ('macro_block', 'macro_expr'):
            node_type, node_name, rule_container = {
                'macro_block': (self.grammar.block, 'macro_expansion', self.grammar.statement_types),
                'macro_expr': (self.grammar.expr, 'macro_expansion', self.grammar.expr_types),
            }[node.type]
            ast = node
            name, rule, macro = make_macro(ast, node_type, node_name, self.grammar)
            rule_container.insert(0, rule)
            self.local_macros[name] = macro
            return Nothing()

        if node.type == 'macro_expansion':
            name, nodes = node
            macro = self.local_macros[name]

            _ = util.Node
            variables = [(var, _('expr', _('identifier', next(gensym)))) for var in macro.variables]
            params = {}
            params.update(dict(nodes))
            params.update(dict(variables))

            env.variables.update(params)
            # TODO: test macro that modifies local scope repeatedly
            return self.evaluate(macro.body, env)

        if node.type == 'unquote':
            node, = node
            expanded = self.evaluate(node, env)
            return self.evaluate(expanded, env)

        if node.type == 'lazy_dictionary':
            kvs = node
            dictionary = {}
            for k,v in kvs:
                key = self.evaluate(k, env)
                value = v
                dictionary[key] = value
            return LazyDictionary(lambda x: self.evaluate(x, env), dictionary)

        if DEBUG:
            debug(expr)
            raise Exception('Unhandled expr type: {}'.format(expr.type))

def dict_join_append(*dicts):
    # TODO: default to not_a_list so we can check if the value was a
    # list because it matched a list or because we've appended to it
    result = collections.defaultdict(list)
    for d in dicts:
        for k, v in d.items():
            if isinstance(v, list):
                result[k].extend(v)
            else:
                result[k] = v
    return result

def make_syntax_rule(syntax, node_type, grammar, multi=False):
    def cb_seq(seq):
        return dict_join_append(*(x for x in seq))

    if isinstance(syntax, list):
        return generator.sequence(
            [make_syntax_rule(s, node_type, grammar) for s in syntax],
            callback=cb_seq,
        )

    if isinstance(syntax, tuple):
        stx, repeat, separator = syntax
        separator = generator.raw(separator[0]) if separator is not None else grammar.ws_all
        rule = generator.sequence(
            [make_syntax_rule(x, node_type, grammar, multi=True) for x in stx],
            callback=cb_seq,
        )
        if repeat == '*':
            return generator.separated_star(rule, separator, callback=cb_seq)
        if repeat == '+':
            return generator.separated_plus(rule, separator, callback=cb_seq)
        if repeat == '?':
            return generator.optional(rule, callback=lambda x: x or {})
        # TODO: numerical repeat specifiers

    if syntax.type == 'string':
        word = syntax[0]
        grammar.reserved_words.add(word)
        return generator.raw(word, callback=lambda x: {})

    if syntax.type == 'identifier':
        name = syntax[0]
        # TODO: use a custom ordered container so as not to conflate the purposes of lists
        return generator.single(node_type, callback=lambda x: {name: [x] if multi else x})

def ast_integer(ast):
    _ = util.Node
    return _('expr', _('number', _('integer', ast)))

def ast_iife(ast):
    _ = util.Node
    return _('expr', _('call', _('expr', _('function', [], ast))))

def make_macro(ast, node_type, supernode_name, grammar):
    name, variables, syntax, body = ast

    name = name[0]
    variables = [var for (var,) in (variables or [])]
    rule = make_syntax_rule(syntax, node_type, grammar)

    def callback(nodes):
        params = {}
        for var, node in nodes.items():
            # TODO: make this check not_a_list to separate multi-match and single-match of a list
            if isinstance(node, list):
                _ = util.Node
                expr = _('expr', _('lazy_dictionary',
                        *[(ast_integer(i), v) for i, v in enumerate(node)]
                ))
                params[var] = expr
            else:
                params[var] = node

        return util.Node(supernode_name, name, params)

    parser_rule = generator.single(rule, callback)
    macro = util.Macro(variables, body)

    return name, parser_rule, macro
