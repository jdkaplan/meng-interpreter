import inspect
import re

from .generator import *
from .util import Node, Namespace, UnaryOperation, BinaryOperation

def ncb(type_):
    return lambda x: Node(type_, x)

def ncb_regex(type_):
    return lambda sre: Node(type_, sre.group())

def ncb_seq(type_, *indices):
    if len(indices) == 0:
        return lambda seq: Node(type_, *(elt for elt in seq))
    return lambda seq: Node(type_, *(seq[i] for i in indices))

def cb_list(seq):
    return [elt for elt in seq]

def cb_keep(*indices):
    return lambda seq: tuple(seq[i] for i in indices)

def log(prefix):
    def logger(arg):
        print(prefix, arg)
        return arg
    return logger

def log_gen(prefix):
    def log(gen):
        yield from map(lambda x: (print(prefix, x), x)[1], gen)
    return log

def apply_hooks(hooks, grammar):
    for hook in hooks:
        hook(grammar)
    return grammar

## Grammar

def Grammar(settings=None):
    if settings is None: settings = {}

    unary_operations = [UnaryOperation(*op) for op in settings['unary_operations']]
    binary_operations = [BinaryOperation(*op) for op in settings['binary_operations']]

    ws_all = regex(r'\s+')
    ws_all_star = regex(r'\s*')
    newline = regex(r'\n+')

    def ws_one_line(source, position):
        matcher = ws_all_star
        for match in parse(matcher, source, position):
            text = match.text
            ws, *_ = re.split(r'\n', text)
            yield Match(ws, position, ws)

    integer = regex(r'\d+', callback=ncb_regex('integer'))
    decimal = regex(r'\d+\.\d+', callback=ncb_regex('decimal'))

    number = choice([decimal, integer], callback=ncb('number'))

    def ncb_string(sre):
        delimiter = sre.group('delimiter')
        content = sre.group('content')
        for escaped, replacement in [
            ('\\' + delimiter, delimiter),
            (r'\b', '\b'),
            (r'\f', '\r'),
            (r'\n', '\n'),
            (r'\r', '\r'),
            (r'\t', '\t'),
            (r'\v', '\v'),
            (r'\\', '\\'),
        ]:
            content = content.replace(escaped, replacement)
        return Node('string', content, delimiter)

    string = regex(
        r'\$(?P<delimiter>\S)'
        r'(?P<content>(\\((?P=delimiter)|[bfnrtv\\])|[^\\])*?)'
        r'(\1)',
        callback=ncb_string,
    )

    keywords = set([
            'if',
            'then',
            'else',
            'function',
            'end',
            'macro_block',
            'macro_expr',
            'syntax',
            'transform',
            'while',
            'do',
    ])

    literals = set([
            'True',
            'False',
            'Nothing',
    ])

    reserved_words = set()
    reserved_substrings = set(['$', '.', ',', '(', ')', '[', ']', '{' , '}', settings['unquote_operator']])
    reserved_substrings |= set([op.symbol for op in unary_operations + binary_operations])

    literal = choice([raw(l, callback=Node) for l in literals])

    def dictionary(source, position):
        def kv_callback(k_v):
            (k, _, v) = k_v
            return (k, v)

        def dictionary_callback(data):
            (_, items, _) = data
            return Node('dictionary', *(kv for kv in items))

        matcher = sequence([
                raw('{'),
                separated_star(
                    sequence([expr, raw(':'), expr], callback=kv_callback),
                    raw(','),
                ),
                raw('}'),
        ], callback=dictionary_callback)

        yield from parse(matcher, source, position)

    def identifier(source, position):
        invalid_names = keywords.union(literals).union(reserved_words)
        operator_symbols = set([op.symbol for op in binary_operations + unary_operations])
        invalid_substrings = reserved_substrings.union(operator_symbols)
        matcher = regex(r'\S+')
        for match in parse(matcher, source, position):
            name = match.text
            for chars in invalid_substrings:
                pattern = re.escape(chars)
                name, *rest = re.split(pattern, name)
            if name and name not in invalid_names:
                yield Match(name, position, Node('identifier', name))

    def function(source, position):
        def function_callback(data):
            (_, _, params, _, body, _) = data
            return Node('function', params, body)
        matcher = sequence([
                raw('function'),
                raw('('),
                separated_star(identifier, raw(','), skip_whitespace=ws_all_star, callback=cb_list),
                raw(')'),
                block,
                raw('end'),
        ], callback=function_callback)

        yield from parse(matcher, source, position)

    # http://www.engr.mun.ca/~theo/Misc/exp_parsing.htm

    def unquote(source, position):
        unq_name = sequence([
                raw(settings['unquote_operator']),
                identifier,
        ],
            skip_whitespace=ws_one_line,
            callback = lambda seq: Node('unquote', Node('expr', seq[1])),
        )
        unq_expr = sequence([
                raw(settings['unquote_operator']),
                raw('('),
                expr,
                raw(')'),
        ],
            callback=ncb_seq('unquote', 2),
        )

        matcher = choice([
                unq_name,
                unq_expr,
        ])
        yield from parse(matcher, source, position)

    def if_statement(source, position):
        matcher = sequence([
                raw('if'),
                block,
                raw('then'),
                block,
                optional(sequence([
                            raw('else'),
                            block,
                ], callback=lambda seq: seq[1]),
                    callback=lambda data: data or Node('block')),
                raw('end'),
        ], callback=lambda seq: Node('if', seq[1], seq[3], seq[4]))
        yield from parse(matcher, source, position)

    def while_statement(source, position):
        matcher = sequence([
                raw('while'),
                block,
                raw('do'),
                block,
                raw('end'),
        ], callback=ncb_seq('while', 1, 3))
        yield from parse(matcher, source, position)

    expr_types = [
        literal,
        number,
        string,
        identifier,
        dictionary,
        function,
        unquote,
        if_statement,
        while_statement,
    ]

    def V(source, position):
        matcher = choice(expr_types)
        yield from parse(matcher, source, position)

    def E(source, position):
        matcher = Exp(0)
        yield from parse(matcher, source, position)

    def P(source, position):
        matcher = sequence([
                choice([
                        U_Exp,
                        sequence([raw('('), E, raw(')')], callback=lambda seq: seq[1]),
                        V,
                ]),
                separated_star(expr_suffix, ws_one_line),
        ],
            skip_whitespace=ws_one_line,
            callback=expr_cb,
        )
        yield from parse(matcher, source, position)

    def B_T1_star(precedence):
        binops = {op.symbol: op for op in binary_operations}

        def rule(source, position):
            matcher_B = choice([
                    raw(op.symbol)
                    for op in binary_operations
            ])
            for match_B in parse(matcher_B, source, position):
                symbol = match_B.text
                tip = position + len(symbol)

                skip_whitespace = ws_one_line
                ws_match = parse(skip_whitespace, source, tip)
                tip += len(next(ws_match).text)

                op = binops[symbol]
                if op.precedence >= precedence:
                    q = {
                        'left': op.precedence + 1,
                        'right': op.precedence,
                    }[op.associativity]
                    matcher_t1 = Exp(q)
                    for match_t1 in parse(matcher_t1, source, tip):
                        end = tip + len(match_t1.text)
                        branch = (match_B.data, match_t1.data)
                        for match_B_T1 in parse(B_T1_star(precedence), source, end):
                            yield Match(
                                source[position:end+len(match_B_T1.text)],
                                position,
                                [branch] + match_B_T1.data,
                            )
                        yield Match(
                            source[position:end],
                            position,
                            [branch],
                        )
        return rule

    def Exp(precedence):
        def rule(source, position):
            matcher_P = P
            for match_P in parse(matcher_P, source, position):
                tip = position + len(match_P.text)
                skip_whitespace = ws_one_line
                ws_match = parse(skip_whitespace, source, tip)
                tip += len(next(ws_match).text)

                for match_B_T1_star in parse(B_T1_star(precedence), source, tip):
                    t = match_P.data
                    rest = match_B_T1_star.data
                    end = tip + len(match_B_T1_star.text)
                    while rest:
                        (b, t1), *rest = rest
                        t = Node('expr', Node('binary_operation', b, t, t1))
                    yield Match(source[position:end], position, t)
                yield match_P
        return rule

    def U_Exp(source, position):
        unops = {op.symbol: op for op in unary_operations}

        matcher_U = choice([
                raw(op.symbol)
                for op in unary_operations
        ])
        for match_U in parse(matcher_U, source, position):
            tip = position + len(match_U.text)

            op = unops[match_U.text]
            q = op.precedence

            skip_whitespace = ws_one_line
            ws_match = parse(skip_whitespace, source, tip)
            tip += len(next(ws_match).text)

            matcher_Exp = Exp(q)
            for match_Exp in parse(matcher_Exp, source, tip):
                end = tip + len(match_Exp.text)
                yield Match(
                    source[position:end],
                    position,
                    Node('unary_operation', match_U.data, match_Exp.data),
                )

    def expr_suffix(source, position):
        call = sequence([
                raw('('),
                ws_all_star,
                separated_star(expr, raw(','), callback=ncb_seq('arguments')),
                ws_all_star,
                raw(')'),
        ],
            callback=lambda seq: seq[2],
            skip_whitespace=ws_one_line,
        )

        access = sequence([
                raw('['),
                ws_all_star,
                expr,
                ws_all_star,
                raw(']'),
        ],
            callback=ncb_seq('key', 2),
            skip_whitespace=ws_one_line,
        )

        matcher = choice([call, access])

        yield from parse(matcher, source, position)

    def expr_cb(seq):
        prefix, suffixes = seq

        node = Node('expr', prefix)
        for suffix in suffixes:
            inner_node = {
                'arguments': lambda prefix, suffix: Node('call', prefix, *suffix.children),
                'key': lambda prefix, suffix: Node('access', prefix, *suffix.children),
            }[suffix.type](node, suffix)
            node = Node('expr', inner_node)
        return node

    expr = E

    macro_vars = optional(
        sequence([
                raw('('),
                separated_star(identifier, raw(','), skip_whitespace=ws_all_star, callback=cb_list),
                raw(')'),
        ], callback=lambda seq: seq[1])
    )

    def syntax_spec_repeat(source, position):
        matcher = sequence([
                raw('('),
                macro_syntax_spec,
                choice([raw(c) for c in '*+?']),
                optional(string),
                raw(')'),
        ], callback=cb_keep(1,2,3))
        yield from parse(matcher, source, position)

    def macro_syntax_spec(source, position):
        matcher = separated_star(choice([
                identifier,
                string,
                syntax_spec_repeat,
        ]), ws_all, cb_list)
        yield from parse(matcher, source, position)

    macro_syntax = sequence([
            raw('syntax'),
            macro_syntax_spec,
    ], callback=lambda seq: seq[1])

    def macro_transform(source, position):
        matcher = sequence([
                raw('transform'),
                block,
        ], callback=lambda seq: seq[1])
        yield from parse(matcher, source, position)

    def macro_cb(seq):
        (kind, name, variables, syntax, transform, _) = seq
        return Node(*(elt for elt in seq[:-1]))

    macro = sequence([
            choice([raw('macro_block'), raw('macro_expr')]),
            identifier,
            macro_vars,
            macro_syntax,
            macro_transform,
            raw('end'),
    ], callback=macro_cb)


    statement_types = [
        macro,
        expr,
    ]

    statement = choice(statement_types, callback=ncb('statement'))

    block = separated_star(
        statement,
        newline,
        skip_whitespace=ws_one_line,
        callback=ncb_seq('block')
    )

    return apply_hooks(settings['grammar_post_create'], Namespace(locals()))
