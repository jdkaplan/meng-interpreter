#!/usr/bin/env python3

import readline
import sys
import traceback
from pprint import pprint

from .generator import make_parser
from .grammar import Grammar
from .evaluator import Evaluator, Environment, Error as EvalError
from . import util

sys.setrecursionlimit(2**30)

class InterpreterError(Exception): pass

def interpret(source, evaluator, environment, grammar):
    parse_statement = make_parser(grammar.statement)
    parse_whitespace = make_parser(grammar.ws_all)

    source = source.strip()
    pos = 0

    while pos < len(source):
        ws = next(parse_whitespace(source, pos))
        if ws is not None:
            pos += len(ws.text)

        match = next(parse_statement(source, pos))

        if match is None:
            raise InterpreterError('syntax error\n{}'.format(source[pos:]))

        ast = match.data
        pos += len(match.text)
        try:
            yield evaluator.evaluate(ast, environment)
        except (EvalError) as e:
            raise InterpreterError('lang error: {}'.format(e))
        except (Exception) as e: # pragma: no cover
            err = sys.exc_info()
            raise InterpreterError('Python error: {}\n{}\n{}'.format(
                    err,
                    ast,
                    ''.join(traceback.format_tb(err[2])),
            ))

def repl(evaluator, environment, grammar, settings):
    while True:                 # pragma: no cover
        try:
            source = input('> ')
        except (EOFError, KeyboardInterrupt):
            return

        if not source:
            continue

        for value in interpret(source, evaluator, environment, grammar):
            print(repr(value))
